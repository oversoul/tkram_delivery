import 'package:rxdart/rxdart.dart';
import 'package:tkram_delivery/models/user.dart';
import 'package:tkram_delivery/providers/repository.dart';

class UsersBloc {
  final _repository = Repository();

  final _usersFetcher = PublishSubject<UserList>();
  final _customUsersFetcher = PublishSubject<CustomUsers>();

  Observable<UserList> get users => _usersFetcher.stream;
  Observable<CustomUsers> get customUsers => _customUsersFetcher.stream;

  Future<void> getAll() async {
    try {
      UserList users = await _repository.getAllUsers();
      _usersFetcher.sink.add(users);
      return;
    } catch (e) {
      _usersFetcher.sink.addError(e);
    }
  }

  Future<void> getUsersByIds(String ids) async {
    try {
      CustomUsers users = await _repository.getUsersByIds(ids);
      _customUsersFetcher.sink.add(users);
      return;
    } catch (e) {
      _customUsersFetcher.sink.addError(e);
    }
  }

  Future<bool> acceptOrder(int orderId) {
    return _repository.acceptOrder(orderId);
  }

  Future<bool> refuseOrder(int orderId) {
    return _repository.refuseOrder(orderId);
  }

  Future<bool> uploadImage(String path) {
    return _repository.uploadImage(path);
  }

  dispose() {
    _usersFetcher.close();
    _customUsersFetcher.close();
    _repository.dispose();
  }
}
