import 'package:rxdart/rxdart.dart';
import 'package:tkram_delivery/models/statement.dart';
import 'package:tkram_delivery/providers/repository.dart';

class StatemenetsBloc {
  Repository _repository = Repository();

  final _statementsFetcher = PublishSubject<StatementList>();

  Observable<StatementList> get statements => _statementsFetcher.stream;

  Future<void> getAll() async {
    try {
      StatementList statements = await _repository.getStatements();
      _statementsFetcher.sink.add(statements);
    } catch (e) {
      _statementsFetcher.sink.addError(e);
    }
  }

  dispose() {
    _statementsFetcher.close();
  }
}
