import 'package:rxdart/rxdart.dart';
import 'package:tkram_delivery/models/price.dart';
import 'package:tkram_delivery/providers/repository.dart';

class PricesBloc {
  final _repository = Repository();

  final _pricesFetcher = PublishSubject<PriceList>();

  Observable<PriceList> get prices => _pricesFetcher.stream;

  Future<void> getAll() async {
    try {
      PriceList prices = await _repository.getAllPrices();
      _pricesFetcher.sink.add(prices);
      return;
    } catch (e) {
      _pricesFetcher.sink.addError(e);
    }
  }

  dispose() {
    _pricesFetcher.close();
    _repository.dispose();
  }
}
