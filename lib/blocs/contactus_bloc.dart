import 'package:tkram_delivery/providers/repository.dart';

class ContactUsBloc {
  final _repository = Repository();

  Future<bool> send(String name, String email, String message) {
    return _repository.sendContactMessage(name, email, message);
  }

  dispose() {
    _repository.dispose();
  }
}
