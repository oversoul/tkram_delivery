import 'package:rxdart/rxdart.dart';
import 'package:tkram_delivery/models/driver.dart';
import 'package:tkram_delivery/providers/repository.dart';

class DriversBloc {
  final _repository = Repository();

  final _favoriteFetch = PublishSubject<bool>();
  final _driverFetcher = PublishSubject<Driver>();
  final _driversFetcher = PublishSubject<DriverList>();
  final _searchFetcher = PublishSubject<SearchDrivers>();
  final _favoritesDriversFetch = PublishSubject<FavDrivers>();

  Observable<Driver> get driver => _driverFetcher.stream;
  Observable<bool> get isFavorite => _favoriteFetch.stream;
  Observable<DriverList> get drivers => _driversFetcher.stream;
  Observable<FavDrivers> get favorites => _favoritesDriversFetch.stream;
  Observable<SearchDrivers> get searchedDrivers => _searchFetcher.stream;

  Future<void> getAll() async {
    try {
      DriverList drivers = await _repository.getAllDrivers();
      _driversFetcher.sink.add(drivers);
    } catch (e) {
      _driversFetcher.sink.addError(e);
    }
  }

  Future<void> search(String keyword) async {
    SearchDrivers drivers = await _repository.searchDrivers(keyword);
    _searchFetcher.sink.add(drivers);
  }

  Future<void> getFavorites() async {
    try {
      FavDrivers drivers = await _repository.getFavoriteDrivers();
      _favoritesDriversFetch.sink.add(drivers);
    } catch (e) {
      _favoritesDriversFetch.sink.addError(e);
    }
  }

  Future<void> getById(int id) async {
    try {
      Driver driver = await _repository.getDriverById(id);
      _driverFetcher.sink.add(driver);
    } catch (e) {
      _driverFetcher.sink.addError(e);
    }
  }

  Future<void> isDriverFavorite(int id) async {
    try {
      bool status = await _repository.isDriverFavorite(id);
      _favoriteFetch.sink.add(status);
    } catch (e) {
      _favoriteFetch.sink.addError(e);
    }
  }

  Future<void> setFavoriteStatus(int id, bool state) async {
    await _repository.setFavoriteStatus(id, state);
  }

  Future<int> createOrder(int clientId, double price) {
    return _repository.createOrder(clientId, price);
  }

  dispose() {
    _favoritesDriversFetch.close();
    _searchFetcher.close();
    _driverFetcher.close();
    _driversFetcher.close();
    _favoriteFetch.close();
    _repository.dispose();
  }
}
