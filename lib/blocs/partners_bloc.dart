import 'package:rxdart/rxdart.dart';
import 'package:tkram_delivery/models/partner.dart';
import 'package:tkram_delivery/providers/repository.dart';

class PartnersBloc {
  final _repository = Repository();

  final _partnersFetcher = PublishSubject<PartnerList>();

  Observable<PartnerList> get partners => _partnersFetcher.stream;

  Future<void> getAll() async {
    try {
      PartnerList partners = await _repository.partners();
      _partnersFetcher.sink.add(partners);
      return;
    } catch (e) {
      _partnersFetcher.sink.addError(e);
    }
  }

  dispose() {
    _partnersFetcher.close();
    _repository.dispose();
  }
}
