import 'package:rxdart/rxdart.dart';
import 'package:tkram_delivery/models/city.dart';
import 'package:tkram_delivery/providers/repository.dart';

class CitiesBloc {
  final _repository = Repository();

  final _citiesFetcher = PublishSubject<CityList>();

  Observable<CityList> get cities => _citiesFetcher.stream;

  Future<void> getAll() async {
    try {
      CityList cities = await _repository.cities();
      _citiesFetcher.sink.add(cities);
    } catch (e) {
      _citiesFetcher.sink.addError(e);
    }
  }

  dispose() {
    _citiesFetcher.close();
    _repository.dispose();
  }
}
