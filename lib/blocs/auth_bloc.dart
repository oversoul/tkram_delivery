import 'package:rxdart/rxdart.dart';
import 'package:tkram_delivery/app_state.dart';
import 'package:tkram_delivery/models/registration_model.dart';
import 'package:tkram_delivery/models/user_model.dart';
import 'package:tkram_delivery/providers/repository.dart';

class AuthBloc {
  final _repository = Repository();

  final _authFetcher = PublishSubject<UserModel>();
  final _availabilityFetcher = PublishSubject<bool>();

  Observable<UserModel> get user => _authFetcher.stream;
  Observable<bool> get availabiliy => _availabilityFetcher.stream;

  Future<bool> login(String phone, String password) {
    return _repository.login(phone, password);
  }

  Future<bool> register(RegistrationModel model) {
    return _repository.register(model);
  }

  Future<bool> isAuthenticated() async {
    final String token = AppState().prefs.getString("token");

    if (token != null) {
      return true;
    }

    return false;
  }

  Future<bool> isAvailable() async {
    final status = await _repository.isAvailable();
    _availabilityFetcher.sink.add(status);
    return status;
  }

  Future<bool> setAvailable(bool value) async {
    final status = await _repository.setAvailable(value);
    // _availabilityFetcher.sink.add(status);
    return status;
  }

  dispose() {
    _authFetcher.close();
    _availabilityFetcher.close();
    _repository.dispose();
  }

  Future<void> isTokenValid() async {
    await _repository.isUserTokenValid();
  }

  Future<bool> verify(String phoneNumber, String verificationCode) {
    return _repository.verify(phoneNumber, verificationCode);
  }

  Future<bool> resend(String phoneNumber) {
    return _repository.resend(phoneNumber);
  }
}
