import 'package:flutter/material.dart';
import 'package:tkram_delivery/app_state.dart';
import 'package:tkram_delivery/models/partner.dart';
import 'package:tkram_delivery/pages/chat/messages_page.dart';
import 'package:tkram_delivery/pages/client/about_us_page.dart';
import 'package:tkram_delivery/pages/client/contact_page.dart';
import 'package:tkram_delivery/pages/client/drivers_page.dart';
import 'package:tkram_delivery/pages/client/favorites_page.dart';
import 'package:tkram_delivery/pages/client/partner_page.dart';
import 'package:tkram_delivery/pages/client/price_list_page.dart';
import 'package:tkram_delivery/pages/client/profile_page.dart';
import 'package:tkram_delivery/pages/client/search_page.dart';
import 'package:tkram_delivery/pages/driver/chat_list_page.dart';
import 'package:tkram_delivery/pages/login_page.dart';
import 'package:tkram_delivery/pages/driver/home_page.dart';
import 'package:tkram_delivery/pages/partners_page.dart';
import 'package:tkram_delivery/pages/settings_page.dart';
import 'package:tkram_delivery/pages/signup_page.dart';
import 'package:tkram_delivery/pages/verification_page.dart';

class FadeRouteTransition<T> extends MaterialPageRoute<T> {
  FadeRouteTransition({
    WidgetBuilder builder,
    RouteSettings settings,
  }) : super(builder: builder, settings: settings);

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    return FadeTransition(
      opacity: animation,
      child: child,
    );
  }
}

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    AppState().currentRoute = settings.name;

    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => LoginPage());
      case '/signup':
        return MaterialPageRoute(builder: (_) => SignupPage());
      case '/auth/confirm':
        return MaterialPageRoute(builder: (_) => VerificationPage());
      case '/about-us':
        return FadeRouteTransition(builder: (_) => AboutUsPage());
      case '/contact-us':
        return FadeRouteTransition(builder: (_) => ContactPage());

      case '/settings':
        return MaterialPageRoute(builder: (_) => SettingsPage());

      case '/home':
        return MaterialPageRoute(builder: (_) => HomePage());

      case '/chats':
        return MaterialPageRoute(builder: (_) => ChatListPage());

      case '/messages':
        final Map<String, String> args = settings.arguments;
        return MaterialPageRoute(
          settings: RouteSettings(name: '/messages'),
          builder: (_) => MessagesPage(args['id'], args['name'], args['image']),
        );

      case '/partner':
        final Map<String, Partner> args = settings.arguments;
        return MaterialPageRoute(builder: (_) => PartnerPage(args['partner']));

      case '/partners':
        return MaterialPageRoute(builder: (_) => PartnersPage());

      case '/drivers':
        return FadeRouteTransition(builder: (_) => DriversPage());
      case '/profile':
        final Map<String, int> args = settings.arguments;
        return MaterialPageRoute(builder: (_) => ProfilePage(args['id']));
      case '/prices':
        return FadeRouteTransition(builder: (_) => PriceListPage());
      case '/favorites':
        return FadeRouteTransition(builder: (_) => FavoritesPage());
      case '/search':
        return FadeRouteTransition(builder: (_) => DriversSearchPage());

      default:
        return MaterialPageRoute(
          builder: (_) => Scaffold(
            body: Center(
              child: Text('No route defined for ${settings.name}'),
            ),
          ),
        );
    }
  }
}
