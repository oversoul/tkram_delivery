import 'package:flutter/material.dart';
import 'package:tkram_delivery/app_state.dart';
import 'package:tkram_delivery/models/city.dart';
import 'package:tkram_delivery/blocs/auth_bloc.dart';
import 'package:tkram_delivery/blocs/cities_bloc.dart';
import 'package:tkram_delivery/pages/verification_page.dart';
import 'package:tkram_delivery/models/registration_model.dart';
import 'package:tkram_delivery/pages/widgets/custom_input.dart';

class SignupPage extends StatefulWidget {
  @override
  _SignupPageState createState() => _SignupPageState();
}

class _SignupPageState extends State<SignupPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  IconButton(
                    icon: Icon(Icons.arrow_back),
                    onPressed: () {
                      AppState().rootNavigator.currentState.pop();
                    },
                  ),
                ],
              ),
              // SizedBox(height: 50),
              _title(),
              SizedBox(height: 30),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 40, vertical: 20),
                child: SignupForm(),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _title() {
    return Container(
      padding: EdgeInsets.only(bottom: 10),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            width: 2,
            color: Color(0xffFAB11F),
          ),
        ),
      ),
      child: Text(
        "CREATE AN ACCOUNT",
        style: TextStyle(
          fontSize: 20,
          color: Colors.grey,
          fontWeight: FontWeight.w600,
        ),
      ),
    );
  }
}

class SignupForm extends StatefulWidget {
  SignupForm({Key key}) : super(key: key);

  @override
  _SignupFormState createState() => _SignupFormState();
}

class _SignupFormState extends State<SignupForm> {
  AuthBloc authBloc;
  CitiesBloc citiesBloc;
  final _formKey = GlobalKey<FormState>();
  RegistrationModel _model = RegistrationModel();

  @override
  void initState() {
    super.initState();
    authBloc = AuthBloc();
    citiesBloc = CitiesBloc();
    citiesBloc.getAll();
  }

  @override
  void dispose() {
    authBloc.dispose();
    citiesBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          CustomInput(
            label: "Name",
            icon: Icons.person,
            validator: (value) {
              if (value.isEmpty) {
                return 'Please enter valid name';
              }
              return null;
            },
            onSaved: (value) => _model.name = value,
          ),
          CustomInput(
            label: "Phone number",
            icon: Icons.phone,
            validator: (value) {
              if (value.isEmpty) {
                return 'Please enter valid phone number';
              }
              return null;
            },
            onSaved: (value) => _model.phone = value,
          ),
          cityField(),
          CustomInput(
            label: "Full Address",
            icon: Icons.home,
            validator: (value) {
              if (value.isEmpty) {
                return 'Please enter valid address';
              }
              return null;
            },
            onSaved: (value) => _model.address = value,
          ),
          CustomInput(
            label: "Password",
            icon: Icons.lock,
            isPassword: true,
            validator: (value) {
              if (value.isEmpty) {
                return 'Please enter valid password';
              }
              return null;
            },
            onSaved: (value) => _model.password = value,
          ),
          CustomInput(
            label: "Confirm Password",
            icon: Icons.lock,
            isPassword: true,
            validator: (value) {
              if (value.isEmpty) {
                return 'Please repeat same password';
              }
              return null;
            },
            onSaved: (value) => _model.confirmationPassword = value,
          ),
          SizedBox(height: 10),
          FlatButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(20),
              ),
            ),
            onPressed: _signUp,
            child: Text("Sign Up"),
            textColor: Colors.white,
            color: Theme.of(context).primaryColor,
          ),
        ],
      ),
    );
  }

  Widget cityField() {
    return StreamBuilder(
      stream: citiesBloc.cities,
      builder: (context, AsyncSnapshot<CityList> snapshot) {
        if (snapshot.hasError) {
          return Center(child: Text("Error! ${snapshot.error}"));
        }

        if (!snapshot.hasData) {
          return Container(
            height: 70,
            child: Center(
              child: SizedBox(
                width: 30,
                height: 30,
                child: CircularProgressIndicator(),
              ),
            ),
          );
        }

        List<DropdownMenuItem<String>> items = [];

        for (var city in snapshot.data.data) {
          items.add(DropdownMenuItem(
            child: Text(city.name, style: TextStyle(color: Color(0xff6D6D6D))),
            value: city.name,
          ));
        }

        return CustomDropDown(
          items: items,
          label: "City",
          value: _model.city,
          icon: Icons.location_city,
          validator: (value) {
            if (value == null) {
              return 'Please enter valid city';
            }
            return null;
          },
          onSaved: (value) => _model.city = value,
        );
      },
    );
  }

  void _signUp() {
    final snackBar = SnackBar(
      content: Text('Please make sure to fill the form correctly.'),
    );
    if (!_formKey.currentState.validate()) {
      Scaffold.of(context).showSnackBar(snackBar);
      return;
    }

    _formKey.currentState.save();

    if (_model.password != _model.confirmationPassword) {
      Scaffold.of(context).showSnackBar(
        SnackBar(content: Text('The password doesn\'t match.')),
      );
      return;
    }

    AppState().globalLoader.show();

    authBloc.register(_model).then((value) {
      AppState().globalLoader.hide();
      print(value);
      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (context) => VerificationPage(),
        ),
      );
    }).catchError((errors) {
      AppState().globalLoader.hide();
      print(errors);
      Scaffold.of(context).showSnackBar(
        SnackBar(content: Text(errors['message'])),
      );
      return;
    });
  }
}
