import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:tkram_delivery/app_state.dart';
import 'package:tkram_delivery/blocs/users_bloc.dart';
import 'package:tkram_delivery/models/user.dart';
import 'package:tkram_delivery/pages/widgets/menu_drawer.dart';

class ChatContact {
  int withId;

  String url;

  ChatContact(this.withId, this.url);
}

class ChatListPage extends StatefulWidget {
  @override
  _ChatListPageState createState() => _ChatListPageState();
}

class _ChatListPageState extends State<ChatListPage> {
  int meId;

  @override
  void initState() {
    meId = AppState().me$.value.id;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Chats"),
      ),
      drawer: MenuDrawer(),
      body: StreamBuilder(
        stream: Firestore.instance.collection('chats').snapshots(),
        builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) {
            return Center(child: Text(snapshot.error.toString()));
          }

          if (!snapshot.hasData) {
            return Center(child: CircularProgressIndicator());
          }

          QuerySnapshot query = snapshot.data;
          List<ChatContact> listOfChats = [];
          List<int> listOfIds = [];
          query.documents.forEach((DocumentSnapshot el) {
            if (el.documentID.startsWith("user$meId-")) {
              var id = el.documentID.replaceFirst("user$meId-user", "");
              var uid = int.parse(id);
              listOfIds.add(uid);
              listOfChats.add(ChatContact(uid, el.documentID));
            } else if (el.documentID.endsWith("-user$meId")) {
              final part = el.documentID.replaceFirst("-user$meId", "");
              var uid = int.parse(part.replaceFirst("user", ""));
              listOfIds.add(uid);
              listOfChats.add(ChatContact(uid, el.documentID));
            }
          });

          print(listOfIds.join(','));

          return UsersListView(
            ids: listOfIds.join(','),
            chats: listOfChats,
          );
        },
      ),
    );
  }
}

class UsersListView extends StatefulWidget {
  final String ids;
  final List<ChatContact> chats;

  UsersListView({this.ids, this.chats});

  @override
  _UsersListViewState createState() => _UsersListViewState();
}

class _UsersListViewState extends State<UsersListView> {
  UsersBloc usersBloc;

  @override
  void initState() {
    usersBloc = UsersBloc();
    usersBloc.getUsersByIds(widget.ids);
    super.initState();
  }

  @override
  void dispose() {
    usersBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: usersBloc.customUsers,
      builder: (context, AsyncSnapshot<CustomUsers> snapshot) {
        if (snapshot.hasError) {
          return Center(child: Text(snapshot.error.toString()));
        }

        if (!snapshot.hasData) {
          return Center(child: CircularProgressIndicator());
        }

        return ListView.builder(
          itemBuilder: (context, index) {
            ChatContact chat = widget.chats[index];
            User user = snapshot.data.data[chat.withId];

            return ListTile(
              onTap: () {
                AppState()
                    .rootNavigator
                    .currentState
                    .pushNamed('/messages', arguments: {
                  'id': "${chat.withId}",
                  'name': user.name,
                  'image': user.image,
                });
              },
              leading: CircleAvatar(
                backgroundImage: user.image != ''
                    ? NetworkImage("http://tkramdelivery.com/${user.image}")
                    : null,
              ),
              title: Text(user.name),
            );
          },
          itemCount: widget.chats.length,
        );
      },
    );
  }
}
