import 'package:flutter/material.dart';
import 'package:tkram_delivery/blocs/statements_bloc.dart';
import 'package:tkram_delivery/models/statement.dart';
import 'package:tkram_delivery/pages/widgets/statement_widget.dart';

class AccountStatementPage extends StatefulWidget {
  @override
  _AccountStatementPageState createState() => _AccountStatementPageState();
}

class _AccountStatementPageState extends State<AccountStatementPage> {
  StatemenetsBloc _statemenetsBloc;

  @override
  void initState() {
    _statemenetsBloc = StatemenetsBloc();
    _statemenetsBloc.getAll();
    super.initState();
  }

  @override
  void dispose() {
    _statemenetsBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("ACCOUNT STATEMENT"),
      ),
      bottomNavigationBar: _total(),
      body: StreamBuilder(
          stream: _statemenetsBloc.statements,
          builder: (context, AsyncSnapshot<StatementList> snapshot) {
            if (snapshot.hasError) {
              return Center(child: Text("Error!\n ${snapshot.error}"));
            }

            if (!snapshot.hasData) {
              return Center(child: CircularProgressIndicator());
            }

            final statements = snapshot.data.statements;

            return ListView.builder(
              itemBuilder: (context, index) {
                final statement = statements[index];
                return StatementRow(
                  order: "#${statement.id}",
                  name: statement.clientName,
                  amount: "${statement.price}",
                );
              },
              itemCount: statements.length,
            );
          }),
    );
  }

  Widget _total() {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.all(15),
      child: StreamBuilder(
        stream: _statemenetsBloc.statements,
        builder: (context, AsyncSnapshot<StatementList> snapshot) {
          if (snapshot.hasError) {
            return Center(child: Text("Error!"));
          }

          if (!snapshot.hasData) {
            return Center(child: CircularProgressIndicator());
          }

          double total = 0;
          for (var stmt in snapshot.data.statements) {
            total += stmt.price;
          }
          return RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w500,
                color: Theme.of(context).primaryColor,
              ),
              text: "Total amount: ",
              children: <TextSpan>[
                TextSpan(
                  text: "$total LBP",
                  style: TextStyle(
                    color: Colors.black.withOpacity(.5),
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
