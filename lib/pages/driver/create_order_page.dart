import 'package:flutter/material.dart';
import 'package:tkram_delivery/helpers.dart';
import 'package:tkram_delivery/models/user.dart';
import 'package:tkram_delivery/blocs/users_bloc.dart';
import 'package:tkram_delivery/blocs/drivers_bloc.dart';
import 'package:tkram_delivery/pages/chat/message_platform.dart';
import 'package:tkram_delivery/pages/widgets/custom_input.dart';
import 'package:autocomplete_textfield/autocomplete_textfield.dart';

class CreateOrderPage extends StatefulWidget {
  @override
  _CreateOrderPageState createState() => _CreateOrderPageState();
}

class _CreateOrderPageState extends State<CreateOrderPage> {
  double price;
  User selected;
  bool showLoader;
  UsersBloc usersBloc;
  DriversBloc driversBloc;
  MessagePlatform _messagePlatform;
  final _formKey = GlobalKey<FormState>();
  final GlobalKey key = GlobalKey<AutoCompleteTextFieldState<User>>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    usersBloc = UsersBloc();
    driversBloc = DriversBloc();
    showLoader = false;
    usersBloc.getAll();
    super.initState();
  }

  @override
  dispose() {
    usersBloc.dispose();
    driversBloc.dispose();
    _messagePlatform.dispose();
    super.dispose();
  }

  Widget _secondPageState() {
    final _border = OutlineInputBorder(
      borderRadius: BorderRadius.all(
        Radius.circular(30),
      ),
      borderSide: BorderSide(
        width: 1,
        color: Theme.of(context).primaryColor,
      ),
    );

    return Container(
      height: 50,
      padding: EdgeInsets.only(left: 10, right: 10),
      child: StreamBuilder(
        stream: usersBloc.users,
        builder: (context, AsyncSnapshot<UserList> snapshot) {
          if (snapshot.hasError) {
            return Center(child: Text("Error!"));
          }

          if (!snapshot.hasData) {
            return Center(child: CircularProgressIndicator());
          }

          return AutoCompleteTextField<User>(
            key: key,
            decoration: InputDecoration(
              hintText: "Client name",
              contentPadding: EdgeInsets.zero,
              disabledBorder: _border,
              enabledBorder: _border,
              errorBorder: _border,
              border: _border,
              filled: true,
              prefixIcon: SizedBox(),
            ),
            itemSubmitted: (item) => setState(() => selected = item),
            suggestions: snapshot.data.data,
            itemBuilder: (context, user) => Padding(
              child: ListTile(title: Text(user.name)),
              padding: EdgeInsets.all(8),
            ),
            itemSorter: (a, b) => 0,
            itemFilter: (suggestion, input) {
              return suggestion.name
                  .toLowerCase()
                  .startsWith(input.toLowerCase());
            },
          );
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 248,
      child: Stack(
        children: <Widget>[
          Scaffold(
            key: _scaffoldKey,
            backgroundColor: Colors.white,
            bottomNavigationBar: _actions(),
            body: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                SizedBox(height: 5),
                _secondPageState(),
                SizedBox(height: 5),
                selectedClient(),
                SizedBox(height: 10),
                Container(
                  padding: EdgeInsets.only(left: 10, right: 10),
                  child: Form(
                    key: _formKey,
                    child: CustomInput(
                      label: "Price",
                      keyboardType: TextInputType.number,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please fill in the price';
                        }
                        return null;
                      },
                      onSaved: (value) {
                        if (value == null) {
                          return;
                        }
                        setState(() => price = double.parse(value));
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
          if (showLoader)
            Container(
              height: 248,
              color: Colors.white.withOpacity(.5),
              child: Center(
                child: CircularProgressIndicator(),
              ),
            ),
        ],
      ),
    );
  }

  Row _actions() {
    return Row(
      children: <Widget>[
        Expanded(
          child: Container(
            width: double.infinity,
            padding: EdgeInsets.only(left: 10, right: 10),
            child: FlatButton(
              color: Theme.of(context).primaryColor,
              child: Icon(Icons.save),
              onPressed: _createOrder,
            ),
          ),
        ),
        FlatButton(
          child: Icon(Icons.close),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ],
    );
  }

  void _createOrder() async {
    _formKey.currentState.validate();
    _formKey.currentState.save();

    if (price == null || selected == null) {
      _scaffoldKey.currentState.showSnackBar(
          SnackBar(content: Text("Please fill the form correctly")));
      return;
    }

    await dismissKeyboard(context);
    setState(() => showLoader = true);

    _messagePlatform = MessagePlatform(selected.id.toString());

    driversBloc.createOrder(selected.id, price).then((value) async {
      setState(() => showLoader = false);
      await _messagePlatform.sendMessage(
        MessageType.Order,
        "New order for the price of: $price LBP",
        price.toInt(),
      );
      Navigator.of(context).pop(true);
      //   Navigator.of(context).push(
      //     MaterialPageRoute(
      //       builder: (context) => VerificationPage(),
      //     ),
      //   );
    }).catchError((errors) {
      setState(() => showLoader = false);
      _scaffoldKey.currentState
          .showSnackBar(SnackBar(content: Text(errors['message'])));
    });
  }

  Widget selectedClient() {
    if (selected == null)
      return Container(
        height: 60,
        width: double.infinity,
        padding: EdgeInsets.all(10),
        child: Center(child: Text("Select a client")),
      );

    return Container(
      height: 60,
      padding: EdgeInsets.all(10),
      width: double.infinity,
      color: Theme.of(context).primaryColor,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text(
            selected.name,
            style: TextStyle(color: Colors.white),
          ),
          IconButton(
            icon: Icon(Icons.cancel, color: Colors.white),
            onPressed: () => setState(() => selected = null),
          ),
        ],
      ),
    );
  }
}

// api/users
