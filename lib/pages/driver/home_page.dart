import 'package:flutter/material.dart';
import 'package:tkram_delivery/blocs/auth_bloc.dart';
import 'package:tkram_delivery/pages/driver/create_order_page.dart';
import 'package:tkram_delivery/pages/driver/account_statement_page.dart';
import 'package:tkram_delivery/pages/widgets/menu_drawer.dart';

class HomePage extends StatelessWidget {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        centerTitle: true,
        title: Text("DASHBOARD"),
      ),
      drawer: MenuDrawer(),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text("AVAILABILITY"),
            SizedBox(height: 50),
            Availability(),
            SizedBox(height: 20),
            FlatButton(
              onPressed: () async {
                final result = await showDialog(
                  context: context,
                  barrierDismissible: false,
                  builder: (_) {
                    return WillPopScope(
                      onWillPop: () async => false,
                      child: SimpleDialog(
                        contentPadding: EdgeInsets.symmetric(
                          vertical: 10,
                          horizontal: 0,
                        ),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(
                            Radius.circular(10),
                          ),
                        ),
                        children: <Widget>[CreateOrderPage()],
                      ),
                    );
                  },
                );

                if (result == true) {
                  _scaffoldKey.currentState.showSnackBar(
                    SnackBar(content: Text("Order created successfully!")),
                  );
                }
              },
              child: Text("Create New Order"),
              color: Theme.of(context).primaryColor,
            ),
            SizedBox(height: 10),
            FlatButton(
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => AccountStatementPage(),
                  ),
                );
              },
              child: Text("Account Statement"),
              color: Theme.of(context).primaryColor,
            ),
          ],
        ),
      ),
    );
  }
}

class Availability extends StatefulWidget {
  @override
  _AvailabilityState createState() => _AvailabilityState();
}

class _AvailabilityState extends State<Availability> {
  bool isAvailable;
  AuthBloc authBloc;

  @override
  void initState() {
    authBloc = AuthBloc();
    _getAvailability();
    super.initState();
  }

  void _getAvailability() async {
    final value = await authBloc.isAvailable();
    setState(() => isAvailable = value);
  }

  @override
  void dispose() {
    authBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (isAvailable == null) {
      return CircularProgressIndicator();
    }

    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            width: 80,
            alignment: Alignment.centerRight,
            child: Text("Busy", style: TextStyle(color: Colors.grey)),
          ),
          Stack(
            children: <Widget>[
              Positioned(
                top: 11.5,
                left: 7.5,
                child: Container(
                  width: 44,
                  height: 26,
                  decoration: BoxDecoration(
                    border: Border.all(
                      width: 1,
                      color: Color(0xffCDCDCD),
                    ),
                    borderRadius: BorderRadius.all(
                      Radius.circular(13),
                    ),
                  ),
                ),
              ),
              Switch(
                value: isAvailable,
                onChanged: (value) async {
                  setState(() => isAvailable = value);
                  authBloc.setAvailable(value).catchError((e) {
                    setState(() => isAvailable = !value);
                    Scaffold.of(context).showSnackBar(
                      SnackBar(
                          content: Text('Couldn\'t change your availability.')),
                    );
                  });
                },
                activeColor: Color(0xff64EA00),
                activeTrackColor: Colors.transparent,
                inactiveTrackColor: Colors.transparent,
                inactiveThumbColor: Color(0xffE70C2A),
              ),
            ],
          ),
          Container(
            width: 80,
            alignment: Alignment.centerLeft,
            child: Text("Available", style: TextStyle(color: Colors.grey)),
          ),
        ],
      ),
    );
  }
}
