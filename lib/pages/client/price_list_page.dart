import 'package:flutter/material.dart';
import 'package:tkram_delivery/blocs/prices_bloc.dart';
import 'package:tkram_delivery/models/price.dart';
import 'package:tkram_delivery/pages/widgets/menu_drawer.dart';

class PriceListPage extends StatefulWidget {
  @override
  _PriceListPageState createState() => _PriceListPageState();
}

class _PriceListPageState extends State<PriceListPage> {
  PricesBloc pricesBloc;

  @override
  void initState() {
    super.initState();
    pricesBloc = PricesBloc();
    pricesBloc.getAll();
  }

  @override
  void dispose() {
    pricesBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: MenuDrawer(),
      appBar: AppBar(
        centerTitle: true,
        title: Text("PRICE LIST"),
      ),
      body: StreamBuilder(
        stream: pricesBloc.prices,
        builder: (context, AsyncSnapshot<PriceList> snapshot) {
          if (snapshot.hasError) {
            return Center(child: Text("Error!"));
          }

          if (!snapshot.hasData) {
            return Center(child: CircularProgressIndicator());
          }

          return ListView.builder(
            itemBuilder: (BuildContext context, int index) {
              return PriceCard(snapshot.data.data[index]);
            },
            itemCount: snapshot.data.data.length,
          );
        },
      ),
    );
  }
}

class PriceCard extends StatelessWidget {
  final Price price;

  const PriceCard(this.price, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      decoration: BoxDecoration(
        border: Border.all(width: 1, color: Colors.grey),
        borderRadius: BorderRadius.all(
          Radius.circular(30),
        ),
        color: Color(0xffF0F0F0),
      ),
      child: ListTile(
        leading: Icon(
          Icons.location_on,
          color: Theme.of(context).primaryColor,
        ),
        title: Text("${price.from} - ${price.to}"),
        trailing: Container(
          decoration: BoxDecoration(
            border: Border(
              left: BorderSide(width: 1, color: Color(0xffFB8D04)),
            ),
          ),
          padding: EdgeInsets.only(left: 15),
          margin: EdgeInsets.symmetric(vertical: 5),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Text(
                price.price.toString(),
                style: TextStyle(
                  fontSize: 17,
                  color: Color(0xffFB8D04),
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                "LBP",
                style: TextStyle(
                  color: Color(0xffFB8D04),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
