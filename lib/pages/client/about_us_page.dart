import 'package:flutter/material.dart';
import 'package:tkram_delivery/pages/widgets/menu_drawer.dart';

class AboutUsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("ABOUT US"),
      ),
      drawer: MenuDrawer(),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(40),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: Text(
                "Introduction",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            Text(
                """“Tkram delivery services” was established in 22nd January 2019 as individual initiative.\n\nOur passion of work and deal with confidence with our customers motivated us to move forward, we have served more than 2000 customers in Tripoli/Lebanon and surround area through one year and half, and we did our best to convince more than 80 local institutions to deal with confidence with us as we work hard to serve their customers as professions which give us high rate and step forward against competitors."""),
            Container(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: Text(
                "Mission",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            Text(
                """Our goal is to provide fast and secured delivery service in Tripoli/Lebanon and surround area and to help individuals and institutions to develop their business. Our services are executed by skilled and qualified employees."""),
            Container(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: Text(
                "Strategy",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            Text(
                """Through all difficulties and challenges, “Tkram delivery services” strive to provide a great service to our valued customers, we also work hard, patiently, positively with a lasting smile to develop an integrated logistic service system that satisfied  the aspiration of our valued customers and give them more chance to invest their time in developing business not to waste their time on clearing matters that we can handle it."""),
            Container(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: Text(
                "Social responsibility",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            Text(
                """We always give the priority for social responsibilities, and we always motivate initiatives and thoughts that have positives effects over all life."""),
            Container(
              padding: EdgeInsets.symmetric(vertical: 20),
              child: Text(
                "The Founder",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            Text(
              "Mohammad Mohammad Nabil El Iaaly",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
    );
  }
}
