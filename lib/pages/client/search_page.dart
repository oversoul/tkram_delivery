import 'package:flutter/material.dart';
import 'package:tkram_delivery/app_state.dart';
import 'package:tkram_delivery/models/driver.dart';
import 'package:tkram_delivery/blocs/drivers_bloc.dart';
import 'package:tkram_delivery/pages/widgets/value_pair.dart';
import 'package:tkram_delivery/pages/widgets/status_widget.dart';

class DriversSearchPage extends StatefulWidget {
  @override
  _DriversSearchPageState createState() => _DriversSearchPageState();
}

class _DriversSearchPageState extends State<DriversSearchPage> {
  DriversBloc driversBloc;
  String keyword;

  @override
  void initState() {
    super.initState();
    driversBloc = DriversBloc();
    driversBloc.search(null);
  }

  @override
  void dispose() {
    driversBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Container(
          height: 35,
          child: TextFormField(
            decoration: InputDecoration(hintText: "Search..."),
            onFieldSubmitted: (value) {
              setState(() {
                driversBloc.search(value);
              });
            },
          ),
        ),
      ),
      // drawer: MenuDrawer(),
      body: StreamBuilder(
        key: GlobalKey(),
        stream: driversBloc.searchedDrivers,
        builder: (context, AsyncSnapshot<SearchDrivers> snapshot) {
          if (snapshot.hasError) {
            return Center(child: Text("Error!"));
          }

          if (!snapshot.hasData) {
            return Center(child: CircularProgressIndicator());
          }

          final drivers = snapshot.data.data;
          if (drivers.length == 0) {
            return Center(
              child: Text("No drivers."),
            );
          }

          return ListView.builder(
            itemBuilder: (context, index) {
              return SearchCard(driver: drivers[index]);
            },
            itemCount: drivers.length,
          );
        },
      ),
    );
  }
}

class SearchCard extends StatelessWidget {
  final Driver driver;

  SearchCard({
    Key key,
    this.driver,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        AppState()
            .rootNavigator
            .currentState
            .pushNamed('/profile', arguments: {'id': driver.id});
      },
      child: Container(
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              color: Colors.grey.withOpacity(.3),
              width: 1,
            ),
          ),
        ),
        child: Row(
          children: <Widget>[
            Container(
              margin: EdgeInsets.symmetric(horizontal: 20),
              child: CircleAvatar(
                radius: 50,
                backgroundImage: driver.avatarUrl != null
                    ? NetworkImage(
                        "http://tkramdelivery.com/${driver.avatarUrl}",
                      )
                    : null,
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Status(available: driver.available),
                ValuePair(name: "Name: ", value: driver.name),
                ValuePair(name: "Joining Since: ", value: driver.joinedSince),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
