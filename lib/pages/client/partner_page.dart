import 'package:flutter/material.dart';
import 'package:tkram_delivery/models/partner.dart';

class PartnerPage extends StatelessWidget {
  final Partner partner;

  PartnerPage(this.partner);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(partner.name),
      ),
      body: Container(
        padding: EdgeInsets.all(40),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              width: 200,
              height: 200,
              padding: EdgeInsets.all(5),
              decoration: BoxDecoration(
                border: Border.all(
                  width: 1,
                  color: Color(0xffF9BD8D),
                ),
                borderRadius: BorderRadius.all(
                  Radius.circular(100),
                ),
              ),
              child: CircleAvatar(
                radius: 180,
                backgroundImage: NetworkImage(partner.logoUrl),
              ),
            ),
            SizedBox(height: 40),
            Container(
              width: double.infinity,
              child: Text(
                partner.descriptionAr,
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(height: 40),
            Text(partner.phoneNumber),
            SizedBox(height: 40),
            if (partner.facebook != null) Text(partner.facebook),
            SizedBox(height: 40),
            if (partner.instagram != null) Text(partner.instagram),
          ],
        ),
      ),
    );
  }
}
