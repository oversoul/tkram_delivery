import 'package:flutter/material.dart';
import 'package:tkram_delivery/app_state.dart';
import 'package:tkram_delivery/blocs/drivers_bloc.dart';
import 'package:tkram_delivery/models/driver.dart';
import 'package:tkram_delivery/pages/widgets/drivers_list.dart';
import 'package:tkram_delivery/pages/widgets/menu_drawer.dart';
import 'package:tkram_delivery/pages/widgets/partner_slider.dart';
import 'package:tkram_delivery/pages/widgets/status_widget.dart';

class DriversPage extends StatefulWidget {
  @override
  _DriversPageState createState() => _DriversPageState();
}

class _DriversPageState extends State<DriversPage> {
  DriversBloc driversBloc;

  @override
  void initState() {
    super.initState();
    driversBloc = DriversBloc();
    driversBloc.getAll();
  }

  @override
  void dispose() {
    driversBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Drivers"),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.search,
              color: Theme.of(context).primaryColor,
            ),
            onPressed: () {
              AppState().rootNavigator.currentState.pushNamed("/search");
            },
          ),
          _priceList(),
          SizedBox(width: 18),
        ],
      ),
      drawer: MenuDrawer(),
      bottomNavigationBar: PartnerSlider(),
      body: StreamBuilder(
        stream: driversBloc.drivers,
        builder: (context, AsyncSnapshot<DriverList> snapshot) {
          if (snapshot.hasError) {
            return Center(child: Text("Error!"));
          }

          if (!snapshot.hasData) {
            return Center(child: CircularProgressIndicator());
          }

          return SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Status(available: true),
                DriversList(snapshot.data.available),
                Status(available: false),
                DriversList(snapshot.data.busy),
              ],
            ),
          );
        },
      ),
    );
  }

  Widget _priceList() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 15),
      child: InkWell(
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(
              Radius.circular(4),
            ),
            color: Color(0xffFC9018),
          ),
          padding: EdgeInsets.all(5),
          child: Text("Price list"),
        ),
        onTap: () {
          AppState().rootNavigator.currentState.pushReplacementNamed('/prices');
        },
      ),
    );
  }
}
