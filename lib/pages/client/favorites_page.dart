import 'package:flutter/material.dart';
import 'package:tkram_delivery/app_state.dart';
import 'package:tkram_delivery/blocs/drivers_bloc.dart';
import 'package:tkram_delivery/models/driver.dart';
import 'package:tkram_delivery/pages/widgets/menu_drawer.dart';
import 'package:tkram_delivery/pages/widgets/status_widget.dart';
import 'package:tkram_delivery/pages/widgets/value_pair.dart';

class FavoritesPage extends StatefulWidget {
  @override
  _FavoritesPageState createState() => _FavoritesPageState();
}

class _FavoritesPageState extends State<FavoritesPage> {
  DriversBloc driversBloc;

  @override
  void initState() {
    super.initState();
    driversBloc = DriversBloc();
    driversBloc.getFavorites();
  }

  @override
  void dispose() {
    driversBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("FAVORITES"),
      ),
      drawer: MenuDrawer(),
      body: StreamBuilder(
        stream: driversBloc.favorites,
        builder: (context, AsyncSnapshot<FavDrivers> snapshot) {
          if (snapshot.hasError) {
            return Center(child: Text("Error!"));
          }

          if (!snapshot.hasData) {
            return Center(child: CircularProgressIndicator());
          }

          final drivers = snapshot.data.data;

          return ListView.builder(
            itemBuilder: (context, index) {
              final driver = drivers[index];
              return Dismissible(
                key: GlobalKey(),
                child: FavoriteCard(driver),
                onDismissed: (direction) async {
                  await driversBloc.setFavoriteStatus(driver.id, false);
                  Scaffold.of(context).showSnackBar(SnackBar(
                    content: Text("Driver ${driver.name} was removed from favorites"),
                  ));
                },
                background: Container(
                  color: Colors.red,
                  padding: EdgeInsets.only(left: 20),
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "Unfavorite",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              );
            },
            itemCount: drivers.length,
          );
        },
      ),
    );
  }
}

class FavoriteCard extends StatelessWidget {
  final Driver driver;

  const FavoriteCard(this.driver, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        AppState()
            .rootNavigator
            .currentState
            .pushNamed('/profile', arguments: {'id': driver.id});
      },
      child: Container(
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              color: Colors.grey.withOpacity(.3),
              width: 1,
            ),
          ),
        ),
        child: Row(
          children: <Widget>[
            Container(
              margin: EdgeInsets.symmetric(horizontal: 20),
              child: CircleAvatar(
                radius: 50,
                backgroundImage: driver.avatarUrl != null
                    ? NetworkImage(
                        "http://tkramdelivery.com/${driver.avatarUrl}",
                      )
                    : null,
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Status(available: driver.available),
                ValuePair(name: "Name: ", value: driver.name),
                ValuePair(name: "Joining Since: ", value: driver.joinedSince),
                SizedBox(height: 10),
                // Container(
                //   height: 30,
                //   child: FlatButton(
                //     color: Theme.of(context).primaryColor,
                //     child: Text("Start Order"),
                //     onPressed: () {},
                //   ),
                // ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
