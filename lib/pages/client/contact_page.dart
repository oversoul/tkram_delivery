import 'package:flutter/material.dart';
import 'package:tkram_delivery/app_state.dart';
import 'package:tkram_delivery/blocs/contactus_bloc.dart';
import 'package:tkram_delivery/pages/widgets/menu_drawer.dart';

class ContactPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("CONTACT US"),
      ),
      drawer: MenuDrawer(),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              width: double.infinity,
              color: Color(0xff1C9FAD),
              padding: EdgeInsets.all(30),
              child: Column(
                children: <Widget>[
                  Icon(Icons.phone),
                  SizedBox(height: 5),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(Icons.location_on, color: Colors.white, size: 16),
                      SizedBox(width: 5),
                      Container(height: 10, width: 1, color: Color(0xffFC8806)),
                      SizedBox(width: 5),
                      Text(
                        "Tripoli-Lebanon",
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                  SizedBox(height: 10),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(Icons.email, color: Colors.white, size: 16),
                      SizedBox(width: 5),
                      Container(height: 10, width: 1, color: Color(0xffFC8806)),
                      SizedBox(width: 5),
                      Text(
                        "Tkram.lb@gmail.com",
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            ContactForm(),
          ],
        ),
      ),
    );
  }
}

class ContactModel {
  String name;
  String email;
  String message;

  static bool isValidEmail(email) {
    return RegExp(
      r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+",
    ).hasMatch(email);
  }
}

class ContactForm extends StatefulWidget {
  const ContactForm({Key key}) : super(key: key);

  @override
  _ContactFormState createState() => _ContactFormState();
}

class _ContactFormState extends State<ContactForm> {
  ContactUsBloc contactBloc;
  final _formKey = GlobalKey<FormState>();
  ContactModel _model = ContactModel();

  @override
  void initState() {
    super.initState();
    contactBloc = ContactUsBloc();
  }

  @override
  void dispose() {
    contactBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(40),
      child: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              "Please contact us using the form below",
              style: TextStyle(
                fontSize: 16,
                color: Color(0xffFC8806),
              ),
            ),
            SizedBox(height: 20),
            TextFormField(
              decoration: InputDecoration(
                contentPadding: EdgeInsets.only(left: 20),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(30),
                  ),
                  borderSide: BorderSide(
                    width: 1,
                    color: Color(0xffD6D6D6),
                  ),
                ),
                filled: true,
                hintText: "Name",
                fillColor: Color(0xffF0F0F0),
              ),
              validator: (value) {
                if (value.isEmpty) {
                  return 'Please enter valid name';
                }
                return null;
              },
              onSaved: (value) => _model.name = value,
            ),
            SizedBox(height: 20),
            TextFormField(
              decoration: InputDecoration(
                contentPadding: EdgeInsets.only(left: 20),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(30),
                  ),
                  borderSide: BorderSide(
                    width: 1,
                    color: Color(0xffD6D6D6),
                  ),
                ),
                filled: true,
                hintText: "Email",
                fillColor: Color(0xffF0F0F0),
              ),
              validator: (value) {
                final msg = 'Please enter valid email address';
                if (value.isEmpty) {
                  return msg;
                }

                if (!ContactModel.isValidEmail(value)) {
                  return msg;
                }
                return null;
              },
              onSaved: (value) => _model.email = value,
            ),
            SizedBox(height: 20),
            TextFormField(
              maxLines: 8,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(20),
                  ),
                  borderSide: BorderSide(
                    width: 1,
                    color: Color(0xffD6D6D6),
                  ),
                ),
                filled: true,
                hintText: "Message",
                fillColor: Color(0xffF0F0F0),
              ),
              validator: (value) {
                if (value.isEmpty) {
                  return 'Please enter valid message';
                }
                return null;
              },
              onSaved: (value) => _model.message = value,
            ),
            SizedBox(height: 20),
            Center(
              child: FlatButton(
                color: Theme.of(context).primaryColor,
                child: Text("Submit"),
                onPressed: _sendMessage,
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _snack(String msg) {
    Scaffold.of(context).showSnackBar(
      SnackBar(content: Text(msg)),
    );
  }

  void _sendMessage() {
    if (!_formKey.currentState.validate()) {
      _snack('Please make sure to fill the form correctly.');
      return;
    }

    _formKey.currentState.save();

    AppState().globalLoader.show();

    contactBloc.send(_model.name, _model.email, _model.message).then((value) {
      AppState().globalLoader.hide();
      _formKey.currentState.reset();
      _snack("Your message was sent successfully.");
    }).catchError((errors) {
      AppState().globalLoader.hide();
      print(errors);
      _snack(errors['message']);
      return;
    });
  }
}
