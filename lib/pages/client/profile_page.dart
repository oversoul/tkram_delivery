import 'package:flutter/material.dart';
import 'package:tkram_delivery/app_state.dart';
import 'package:tkram_delivery/blocs/drivers_bloc.dart';
import 'package:tkram_delivery/models/driver.dart';
import 'package:tkram_delivery/pages/widgets/status_widget.dart';
import 'package:tkram_delivery/pages/widgets/value_pair.dart';

class ProfilePage extends StatefulWidget {
  final int id;

  ProfilePage(this.id);

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage>
    with TickerProviderStateMixin {
  final bool isStarred = true;
  TabController _tabController;
  DriversBloc driversBloc;

  @override
  void initState() {
    _tabController = new TabController(vsync: this, length: 2);
    super.initState();
    driversBloc = DriversBloc();
    driversBloc.getById(widget.id);
    driversBloc.isDriverFavorite(widget.id);
  }

  @override
  void dispose() {
    _tabController.dispose();
    driversBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Profile"),
        actions: <Widget>[
          StreamBuilder(
            stream: driversBloc.isFavorite,
            builder: (context, AsyncSnapshot<bool> snapshot) {
              if (snapshot.hasError) {
                return Center(
                  child: Text("Error!", style: TextStyle(color: Colors.black)),
                );
              }

              if (!snapshot.hasData) {
                return Center(child: CircularProgressIndicator());
              }

              return IconButton(
                icon: snapshot.data
                    ? Icon(Icons.star, color: Color(0xffFECB31))
                    : Icon(Icons.star_border),
                onPressed: () async {
                  final state = await _toggleFavorite(snapshot.data);
                  String msg = "Driver was added to your favorites list";
                  if (state) {
                    msg = "Driver was removed from your favorites list";
                  }

                  Scaffold.of(context)
                      .showSnackBar(SnackBar(content: Text(msg)));
                },
              );
            },
          ),
          SizedBox(width: 18),
        ],
      ),
      body: StreamBuilder(
        stream: driversBloc.driver,
        builder: (context, AsyncSnapshot<Driver> snapshot) {
          if (snapshot.hasError) {
            final Map<String, dynamic> errors = snapshot.error;
            print(errors);
            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text("Error!", style: TextStyle(fontSize: 20)),
                  SizedBox(height: 20),
                  // Text(errors['message']),
                ],
              ),
            );
          }

          if (!snapshot.hasData) {
            return Center(child: CircularProgressIndicator());
          }

          return Center(
            child: Column(
              children: <Widget>[
                SizedBox(height: 10),
                Container(
                  width: 200,
                  height: 200,
                  padding: EdgeInsets.all(5),
                  decoration: BoxDecoration(
                    border: Border.all(
                      width: 1,
                      color: Color(0xffF9BD8D),
                    ),
                    borderRadius: BorderRadius.all(
                      Radius.circular(100),
                    ),
                  ),
                  child: CircleAvatar(
                    radius: 180,
                    backgroundImage: snapshot.data.avatarUrl != null
                        ? NetworkImage(
                            "http://tkramdelivery.com/${snapshot.data.avatarUrl}",
                          )
                        : null,
                  ),
                ),
                Status(available: snapshot.data.available),
                FlatButton(
                  color: Theme.of(context).primaryColor,
                  child: Text("Start order"),
                  onPressed: () {
                    AppState().rootNavigator.currentState.pushNamed('/messages',
                        arguments: {
                          'id': "${widget.id}",
                          'name': snapshot.data.name,
                          'image': snapshot.data.imageUrl
                        });
                  },
                ),
                Expanded(
                  child: Column(
                    children: <Widget>[
                      Container(
                        color: Color(0xffF2F2F2),
                        child: TabBar(
                          controller: _tabController,
                          labelColor: Color(0xff9E9E9E),
                          indicatorColor: Color(0xffF6C7A2),
                          indicatorSize: TabBarIndicatorSize.tab,
                          tabs: [
                            Tab(child: Text("DRIVER INFO")),
                            Tab(child: Text("MOTORCYCLE INFO")),
                          ],
                        ),
                      ),
                      Expanded(
                        child: TabBarView(
                          controller: _tabController,
                          children: <Widget>[
                            _driverInfo(snapshot.data),
                            _bikeInfo(),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }

  Widget _driverInfo(Driver driver) {
    return Padding(
      padding: EdgeInsets.all(10),
      child: Column(
        children: <Widget>[
          ValuePair(name: "Name: ", value: driver.name),
          ValuePair(name: "Joined Since: ", value: driver.joinedSince),
          SizedBox(height: 40),
          Text("Rating"),
          FlatButton(
            textColor: Colors.white,
            color: Color(0xffFCB040),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Icon(Icons.star),
                Text("200 reviews"),
              ],
            ),
            onPressed: () {},
          ),
        ],
      ),
    );
  }

  Widget _bikeInfo() {
    return Padding(
      padding: EdgeInsets.all(10),
      child: Row(
        children: <Widget>[
          Container(
            width: 150,
            height: 150,
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(
              border: Border.all(
                width: 1,
                color: Colors.orange,
              ),
            ),
          ),
          SizedBox(width: 10),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text("Food Delivery Motorcycle With Box"),
                SizedBox(height: 10),
                Text("T-888 678"),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Future<bool> _toggleFavorite(bool state) async {
    await driversBloc.setFavoriteStatus(widget.id, !state);
    await driversBloc.isDriverFavorite(widget.id);
    return state;
  }
}
