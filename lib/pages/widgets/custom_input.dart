import 'package:flutter/material.dart';

class CustomInput extends StatelessWidget {
  final String label;
  final IconData icon;
  final bool isPassword;
  final TextInputType keyboardType;
  final void Function(String) onSaved;
  final String Function(String) validator;

  const CustomInput({
    Key key,
    this.icon,
    this.onSaved,
    this.validator,
    @required this.label,
    this.isPassword = false,
    this.keyboardType = TextInputType.text,
  }) : super(key: key);

  OutlineInputBorder inputBorder(BuildContext context,
      [bool isFocused = false]) {
    return OutlineInputBorder(
      borderRadius: BorderRadius.all(
        Radius.circular(30),
      ),
      borderSide: BorderSide(
        width: 1,
        color: isFocused ? Theme.of(context).primaryColor : Color(0xffD6D6D6),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    OutlineInputBorder _border = inputBorder(context);

    return Container(
      height: 70,
      child: TextFormField(
        obscureText: isPassword,
        validator: validator,
        onSaved: onSaved,
        keyboardType: keyboardType,
        decoration: InputDecoration(
          hintText: label,
          prefixIcon: (icon != null)
              ? Icon(
                  icon,
                  size: 18,
                  color: Theme.of(context).primaryColor,
                )
              : SizedBox(),
          contentPadding: EdgeInsets.zero,
          disabledBorder: _border,
          enabledBorder: _border,
          focusedBorder: inputBorder(context, true),
          errorBorder: _border,
          border: _border,
          filled: true,
        ),
      ),
    );
  }
}

class CustomDropDown extends StatelessWidget {
  final String label;
  final String value;
  final IconData icon;
  final TextInputType keyboardType;
  final void Function(String) onSaved;
  final String Function(String) validator;
  final List<DropdownMenuItem<String>> items;

  const CustomDropDown({
    Key key,
    this.icon,
    this.onSaved,
    this.validator,
    @required this.value,
    @required this.items,
    @required this.label,
    this.keyboardType = TextInputType.text,
  }) : super(key: key);

  OutlineInputBorder inputBorder(BuildContext context,
      [bool isFocused = false]) {
    return OutlineInputBorder(
      borderRadius: BorderRadius.all(
        Radius.circular(30),
      ),
      borderSide: BorderSide(
        width: 1,
        color: isFocused ? Theme.of(context).primaryColor : Color(0xffD6D6D6),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    OutlineInputBorder _border = inputBorder(context);

    return Container(
      height: 70,
      child: DropdownButtonFormField<String>(
        value: value,
        items: items,
        validator: validator,
        icon: Row(
          children: [
            Icon(Icons.arrow_drop_down),
            SizedBox(width: 10),
          ],
        ),
        decoration: InputDecoration(
          hintText: label,
          prefixIcon: (icon != null)
              ? Icon(
                  icon,
                  size: 18,
                  color: Theme.of(context).primaryColor,
                )
              : SizedBox(),
          contentPadding: EdgeInsets.zero,
          disabledBorder: _border,
          enabledBorder: _border,
          focusedBorder: inputBorder(context, true),
          errorBorder: _border,
          border: _border,
          filled: true,
        ),
        onChanged: onSaved,
      ),
    );
  }
}
