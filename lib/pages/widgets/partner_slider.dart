import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:tkram_delivery/app_state.dart';
import 'package:tkram_delivery/blocs/partners_bloc.dart';
import 'package:tkram_delivery/models/partner.dart';

class PartnerSlider extends StatefulWidget {
  const PartnerSlider({Key key}) : super(key: key);

  @override
  _PartnerSliderState createState() => _PartnerSliderState();
}

class _PartnerSliderState extends State<PartnerSlider> {
  PartnersBloc partnersBloc;

  @override
  void initState() {
    partnersBloc = PartnersBloc();
    partnersBloc.getAll();
    super.initState();
  }

  @override
  void dispose() {
    partnersBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final defaultHeight = MediaQuery.of(context).size.height / 6 - 10;

    return Container(
      height: defaultHeight,
      // color: Colors.white,
      child: StreamBuilder(
        stream: partnersBloc.partners,
        builder: (context, AsyncSnapshot<PartnerList> snapshot) {
          if (snapshot.hasError) {
            return Center(
              child: Text(snapshot.error.toString()),
            );
          }

          if (!snapshot.hasData) {
            return Center(child: CircularProgressIndicator());
          }

          return CarouselSlider(
            options: CarouselOptions(
              autoPlay: true,
              autoPlayInterval: Duration(seconds: 3),
            ),
            items: snapshot.data.all.map((Partner item) {
              return InkWell(
                onTap: () {
                  AppState()
                      .rootNavigator
                      .currentState
                      .pushNamed('/partner', arguments: {'partner': item});
                },
                child: Container(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Container(
                        height: defaultHeight - 30,
                        child: Image(image: NetworkImage(item.logoUrl)),
                      ),
                      SizedBox(height: 5),
                      Text(item.name),
                    ],
                  ),
                ),
              );
            }).toList(),
          );
        },
      ),
    );
  }
}
