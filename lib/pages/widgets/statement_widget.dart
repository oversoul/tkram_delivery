import 'package:flutter/material.dart';
import 'package:tkram_delivery/pages/widgets/value_pair.dart';

class StatementRow extends StatelessWidget {
  final String name;
  final String order;
  final String amount;

  StatementRow({
    @required this.order,
    @required this.amount,
    @required this.name,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(color: Colors.grey.withOpacity(.7), width: .5),
        ),
      ),
      padding: EdgeInsets.symmetric(horizontal: 40, vertical: 20),
      child: Column(
        children: <Widget>[
          ValuePair(name: "Order number: ", value: order),
          SizedBox(height: 5),
          ValuePair(name: "Order amount: ", value: "$amount LBP"),
          SizedBox(height: 5),
          ValuePair(name: "Client name: ", value: name),
        ],
      ),
    );
  }
}
