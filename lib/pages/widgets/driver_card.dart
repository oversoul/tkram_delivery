import 'package:flutter/material.dart';
import 'package:tkram_delivery/app_state.dart';
import 'package:tkram_delivery/models/driver.dart';

class DriverCard extends StatelessWidget {
  final Driver driver;

  const DriverCard({Key key, @required this.driver}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    NetworkImage image = driver.avatarUrl == null
        ? null
        : NetworkImage("http://tkramdelivery.com/${driver.avatarUrl}");
    return Container(
      padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
      margin: EdgeInsets.only(bottom: 10),
      decoration: BoxDecoration(
        color: Color(0xff94CED5),
        borderRadius: BorderRadius.all(
          Radius.circular(60),
        ),
      ),
      child: ListTile(
        onTap: () {
          AppState()
              .rootNavigator
              .currentState
              .pushNamed('/profile', arguments: {'id': driver.id});
        },
        contentPadding: EdgeInsets.zero,
        title: Row(
          children: <Widget>[
            CircleAvatar(
              radius: 38,
              backgroundImage: image,
            ),
            SizedBox(width: 10),
            Expanded(
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      driver.name,
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      driver.description,
                      style: TextStyle(
                        fontSize: 14,
                        color: Colors.white,
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
