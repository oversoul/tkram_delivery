import 'package:flutter/material.dart';
import 'package:tkram_delivery/app_state.dart';
import 'package:tkram_delivery/models/user_model.dart';

class MenuDrawer extends StatelessWidget {
  @override
  Drawer build(BuildContext context) {
    final avatar = AppState().prefs.getString('avatar');

    return Drawer(
      elevation: 0,
      child: StreamBuilder(
        stream: AppState().me$.stream,
        builder: (context, AsyncSnapshot<Profile> snapshot) {
          if (snapshot.hasError) {
            return Center(
              child: Text("Couldn't get profile"),
            );
          }

          if (!snapshot.hasData) {
            return Center(child: CircularProgressIndicator());
          }

          Profile profile = snapshot.data;

          return ListView(
            children: <Widget>[
              DrawerHeader(
                child: Row(
                  children: [
                    CircleAvatar(
                      radius: 60,
                      backgroundImage: NetworkImage(
                        "http://tkramdelivery.com/$avatar",
                      ),
                    ),
                    SizedBox(width: 10),
                    Text(
                      profile?.name,
                      style: TextStyle(color: Colors.white),
                    ),
                  ],
                ),
                decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                ),
              ),
              ..._getMenuItems(profile?.type),
              ListTile(
                leading: Icon(Icons.settings),
                title: Text("Settings"),
                onTap: () => _goTo('/settings'),
              ),
              ListTile(
                leading: Icon(Icons.bubble_chart),
                title: Text('Partners'),
                onTap: () => _goTo('/partners'),
              ),
              ListTile(
                leading: Icon(Icons.info_outline),
                title: Text('About Us'),
                onTap: () => _goTo('/about-us'),
              ),
              ListTile(
                  leading: Icon(Icons.mail_outline),
                  title: Text('Contact Us'),
                  onTap: () => _goTo('/contact-us')),
              ListTile(
                  leading: Icon(Icons.power_settings_new),
                  title: Text('Logout'),
                  onTap: () => AppState().logout()),
            ],
          );
        },
      ),
    );
  }

  List<Widget> _getMenuItems(String type) {
    if (type == "driver") {
      return <Widget>[
        ListTile(
            leading: Icon(Icons.home),
            title: Text('Home'),
            onTap: () => _goTo("/home")),
        ListTile(
            leading: Icon(Icons.chat),
            title: Text('Chats'),
            onTap: () => _goTo("/chats")),
      ];
    }

    return <Widget>[
      ListTile(
          leading: Icon(Icons.motorcycle),
          title: Text('Drivers'),
          onTap: () => _goTo("/drivers")),
      ListTile(
          leading: Icon(Icons.favorite_border),
          title: Text('My Favorites'),
          onTap: () => _goTo("/favorites")),
      ListTile(
          leading: Icon(Icons.attach_money),
          title: Text('Price List'),
          onTap: () => _goTo("/prices")),
    ];
  }

  _goTo(String path) {
    AppState().rootNavigator.currentState.pushReplacementNamed(path);
  }
}
