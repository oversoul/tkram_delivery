import 'package:flutter/material.dart';

class Status extends StatelessWidget {
  final bool available;

  Status({@required this.available});

  Widget _statusCircle() {
    return Container(
      width: 10,
      height: 10,
      margin: EdgeInsets.only(right: 10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(5),
        ),
        color: available ? Color(0xff64EA02) : Color(0xffE50E28),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          _statusCircle(),
          Text(
            available ? "AVAILABLE" : "BUSY",
            style: TextStyle(
              fontSize: 16,
              color: Color(0xff969696),
              fontWeight: FontWeight.w600,
            ),
          ),
        ],
      ),
    );
  }
}
