import 'package:flutter/material.dart';
import 'package:tkram_delivery/models/driver.dart';
import 'package:tkram_delivery/pages/widgets/driver_card.dart';

class DriversList extends StatelessWidget {
  final List<Driver> drivers;

  DriversList([data]) : this.drivers = data ?? List<Driver>();

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      height: MediaQuery.of(context).size.height / 3 - 10,
      margin: EdgeInsets.symmetric(horizontal: 20),
      decoration: BoxDecoration(
        color: Color(0xff5FBAC4),
        borderRadius: BorderRadius.all(
          Radius.circular(30),
        ),
      ),
      child: _driversList(),
    );
  }

  Widget _driversList() {
    if (drivers.length == 0) {
      return Center(
        child: Text(
          "No drivers",
          style: TextStyle(color: Colors.white),
        ),
      );
    }
    return ListView.builder(
      itemBuilder: (context, index) {
        return DriverCard(driver: drivers[index]);
      },
      itemCount: drivers.length,
    );
  }
}
