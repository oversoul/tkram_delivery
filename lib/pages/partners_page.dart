import 'package:flutter/material.dart';
import 'package:tkram_delivery/app_state.dart';
import 'package:tkram_delivery/blocs/partners_bloc.dart';
import 'package:tkram_delivery/models/partner.dart';
import 'package:tkram_delivery/pages/widgets/menu_drawer.dart';

class PartnersPage extends StatefulWidget {
  const PartnersPage({Key key}) : super(key: key);

  @override
  _PartnersPageState createState() => _PartnersPageState();
}

class _PartnersPageState extends State<PartnersPage> {
  PartnersBloc partnersBloc;

  @override
  void initState() {
    partnersBloc = PartnersBloc();
    partnersBloc.getAll();
    super.initState();
  }

  @override
  void dispose() {
    partnersBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Partners"),
      ),
      drawer: MenuDrawer(),
      body: StreamBuilder(
        stream: partnersBloc.partners,
        builder: (context, AsyncSnapshot<PartnerList> snapshot) {
          if (snapshot.hasError) {
            return Center(
              child: Text(snapshot.error.toString()),
            );
          }

          if (!snapshot.hasData) {
            return Center(child: CircularProgressIndicator());
          }

          return ListView.builder(
            itemCount: snapshot.data.all.length,
            itemBuilder: (context, index) {
              final Partner item = snapshot.data.all[index];
              return InkWell(
                onTap: () {
                  AppState()
                      .rootNavigator
                      .currentState
                      .pushNamed('/partner', arguments: {'partner': item});
                },
                child: ListTile(
                  leading: CircleAvatar(
                    backgroundImage: NetworkImage(item.logoUrl),
                  ),
                  title: Text(item.name),
                ),
              );
            },
          );
        },
      ),
    );
  }
}
