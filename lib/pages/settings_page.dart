import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:tkram_delivery/app_state.dart';
import 'package:tkram_delivery/blocs/users_bloc.dart';
import 'package:tkram_delivery/pages/widgets/menu_drawer.dart';

class SettingsPage extends StatefulWidget {
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  File file;
  String defaultImage;
  UsersBloc usersBloc;

  @override
  void initState() {
    usersBloc = UsersBloc();
    defaultImage = AppState().prefs.getString('avatar');
    super.initState();
  }

  @override
  void dispose() {
    usersBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("SETTINGS"),
        actions: [
          FlatButton(
            child: Text("Save"),
            onPressed: _upload,
          )
        ],
      ),
      drawer: MenuDrawer(),
      body: Column(
        children: <Widget>[
          SizedBox(height: 10),
          Center(
            child: Container(
              width: 200,
              height: 200,
              padding: EdgeInsets.all(5),
              decoration: BoxDecoration(
                border: Border.all(
                  width: 1,
                  color: Color(0xffF9BD8D),
                ),
                borderRadius: BorderRadius.all(
                  Radius.circular(100),
                ),
              ),
              child: CircleAvatar(
                radius: 180,
                backgroundImage: file != null
                    ? FileImage(file)
                    : NetworkImage("http://tkramdelivery.com/$defaultImage"),
              ),
            ),
          ),
          SizedBox(height: 40),
          RaisedButton(
            onPressed: _choose,
            child: Text('Choose Image'),
          ),
        ],
      ),
    );
  }

  void _choose() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() => file = image);
  }

  void _upload() async {
    if (file == null) return;

    AppState().globalLoader.show();
    var result = await usersBloc.uploadImage(file.path);
    var snackBar = SnackBar(content: Text("Couldn't update the image."));

    if (result == true) {
      snackBar = SnackBar(content: Text("Image updated."));
    }

    AppState().globalLoader.hide();
    Scaffold.of(context).showSnackBar(snackBar);
  }
}
