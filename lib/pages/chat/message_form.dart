import 'dart:async';
import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_audio_recorder/flutter_audio_recorder.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:tkram_delivery/pages/chat/message_platform.dart';


class MessageSendForm extends StatefulWidget {
  final String themId;
  final String chatSlug;
  final String themName;

  const MessageSendForm(this.chatSlug, this.themId, this.themName, {Key key})
      : super(key: key);

  @override
  _MessageSendFormState createState() => _MessageSendFormState();
}

class _MessageSendFormState extends State<MessageSendForm> {
  Recording _current;
  bool showSend = false;
  FlutterAudioRecorder _recorder;
  TextEditingController _controller;
  MessagePlatform _messagePlatform;
  RecordingStatus _currentStatus = RecordingStatus.Unset;

  @override
  void initState() {
    _controller = TextEditingController();
    _init();
    _messagePlatform = MessagePlatform(widget.themId);
    super.initState();
  }

  _init() async {
    try {
      if (await FlutterAudioRecorder.hasPermissions) {
        String customPath = '/recording_';
        Directory appDocDirectory;
        if (Platform.isIOS) {
          appDocDirectory = await getApplicationDocumentsDirectory();
        } else {
          appDocDirectory = await getExternalStorageDirectory();
        }

        customPath = appDocDirectory.path +
            customPath +
            DateTime.now().millisecondsSinceEpoch.toString();

        // AudioFormat is optional, if given value, will overwrite path extension when there is conflicts.
        _recorder = FlutterAudioRecorder(
          customPath,
          audioFormat: AudioFormat.WAV,
        );

        await _recorder.initialized;
        // after initialization
        var current = await _recorder.current(channel: 0);

        // should be "Initialized", if all working fine
        setState(() {
          _current = current;
          _currentStatus = current.status;
        });
      } else {
        Scaffold.of(context).showSnackBar(
          SnackBar(content: Text("You must accept permissions")),
        );
      }
    } catch (e) {
      print(e);
    }
  }

  _start() async {
    try {
      await _recorder.start();
      var recording = await _recorder.current(channel: 0);
      setState(() => _current = recording);

      const tick = Duration(milliseconds: 50);
      Timer.periodic(tick, (Timer t) async {
        if (_currentStatus == RecordingStatus.Stopped) {
          t.cancel();
        }

        var current = await _recorder.current(channel: 0);
        // print(current.status);
        setState(() {
          _current = current;
          _currentStatus = _current.status;
        });
      });
    } catch (e) {
      print(e);
    }
  }

  void _stop() async {
    var result = await _recorder.stop();

    File audioFile = File(result.path);
    if (result.duration.inSeconds < 1) {
      audioFile.delete();
      print("deleted");
      return;
    }

    uploadFile(audioFile, MessageType.Audio);

    setState(() {
      _current = result;
      _currentStatus = _current.status;
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    _messagePlatform.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 5, right: 5, bottom: 5),
      decoration: BoxDecoration(
        color: Theme.of(context).primaryColor,
        borderRadius: BorderRadius.all(
          Radius.circular(40),
        ),
      ),
      child: Row(
        children: [
          InkWell(
            child: Container(
              height: 40,
              width: 40,
              child: Icon(
                Icons.image,
                color: Colors.white,
              ),
            ),
            onTap: _getImage,
          ),
          Expanded(
            child: inputSpace(),
          ),
          _formSubmit(),
        ],
      ),
    );
  }

  Widget inputSpace() {
    if (_currentStatus == RecordingStatus.Recording) {
      final duration = _current.duration.toString().split(".").first;

      return Container(
        height: 48,
        alignment: Alignment.centerLeft,
        padding: EdgeInsets.all(8.0),
        child: Text(
          "$duration Recording...",
          style: TextStyle(color: Colors.white, fontSize: 16),
        ),
      );
    }

    return TextFormField(
      controller: _controller,
      decoration: InputDecoration(
        border: InputBorder.none,
        hintText: "Type a message",
        hintStyle: TextStyle(color: Colors.white),
      ),
      onChanged: (value) {
        setState(() => showSend = value.isNotEmpty);
      },
    );
  }

  Widget _formSubmit() {
    if (showSend) {
      return InkWell(
        child: Container(
          width: 40,
          height: 40,
          child: Icon(Icons.send, color: Colors.white),
        ),
        onTap: () {
          final value = _controller.text.trim();
          // FocusScope.of(context).unfocus();
          if (value == '') return;

          _controller.text = '';

          setState(() => showSend = false);
          _messagePlatform.sendMessage(MessageType.Text, value);
        },
      );
    }

    PointerUpEvent();

    return GestureDetector(
      child: Container(
        width: 40,
        height: 40,
        child: Icon(Icons.mic, color: Colors.white),
      ),
      onTapDown: (_) async {
        await _init();
        _start();
      },
      onTapCancel: () {
        if (_currentStatus != RecordingStatus.Unset) {
          _stop();
        }
      },
      onTapUp: (_) {
        if (_currentStatus != RecordingStatus.Unset) {
          _stop();
        }
      },
    );
  }

  Future _getImage() async {
    // picker
    File imageFile = await ImagePicker.pickImage(
      source: ImageSource.gallery,
      imageQuality: 50,
    );

    if (imageFile != null) {
      // setState(() {
      // isLoading = true;
      // });
      uploadFile(imageFile, MessageType.Image);
    }
  }

  Future uploadFile(File imageFile, MessageType type) async {
    String fileName = DateTime.now().millisecondsSinceEpoch.toString();
    StorageReference reference = FirebaseStorage.instance.ref().child(fileName);
    StorageUploadTask uploadTask = reference.putFile(imageFile);
    StorageTaskSnapshot storageTaskSnapshot = await uploadTask.onComplete;

    final doc =
        await _messagePlatform.sendMessage(MessageType.Loading, '');

    storageTaskSnapshot.ref.getDownloadURL().then((fileUrl) {
      doc.updateData({
        'message': fileUrl,
        'type': type.value,
      });
    }, onError: (err) {
      print('This file $err');
    });
  }
}
