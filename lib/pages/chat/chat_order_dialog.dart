import 'package:flutter/material.dart';
import 'package:tkram_delivery/blocs/drivers_bloc.dart';
import 'package:tkram_delivery/helpers.dart';
import 'package:tkram_delivery/pages/chat/message_platform.dart';
import 'package:tkram_delivery/pages/widgets/custom_input.dart';

class OrderDialog extends StatefulWidget {
  final int clientId;

  OrderDialog(this.clientId);

  @override
  _OrderDialogState createState() => _OrderDialogState();
}

class _OrderDialogState extends State<OrderDialog> {
  double price;
  bool showLoader;
  DriversBloc driversBloc;
  MessagePlatform _messagePlatform;
  final _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    driversBloc = DriversBloc();
    _messagePlatform = MessagePlatform(widget.clientId.toString());
    showLoader = false;
    super.initState();
  }

  @override
  dispose() {
    driversBloc.dispose();
    _messagePlatform.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.transparent,
      body: Stack(
        children: [
          SimpleDialog(
            contentPadding: EdgeInsets.all(10),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(10),
              ),
            ),
            children: <Widget>[
              Form(
                key: _formKey,
                child: CustomInput(
                  label: "Price",
                  keyboardType: TextInputType.number,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please fill in the price';
                    }
                    return null;
                  },
                  onSaved: (value) =>
                      setState(() => price = double.parse(value)),
                ),
              ),
              _actions(),
            ],
          ),
          if (showLoader)
            Container(
              color: Colors.white.withOpacity(.5),
              child: Center(
                child: CircularProgressIndicator(),
              ),
            ),
        ],
      ),
    );
  }

  Widget _actions() {
    return Row(
      children: <Widget>[
        Expanded(
          child: Container(
            width: double.infinity,
            child: FlatButton(
              color: Theme.of(context).primaryColor,
              child: Text("Create order"),
              onPressed: _createOrder,
            ),
          ),
        ),
        FlatButton(
          child: Icon(Icons.close),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ],
    );
  }

  void _createOrder() async {
    _formKey.currentState.validate();
    _formKey.currentState.save();

    if (price == null) {
      _scaffoldKey.currentState.showSnackBar(
        SnackBar(content: Text("Please fill the form correctly")),
      );
      return;
    }

    await dismissKeyboard(context);
    setState(() => showLoader = true);

    driversBloc.createOrder(widget.clientId, price).then((value) async {
      setState(() => showLoader = false);
      await _messagePlatform.sendMessage(
        MessageType.Order,
        "New order for the price of: $price LBP",
        value,
      );
      Navigator.of(context).pop(true);
    }).catchError((errors) {
      setState(() => showLoader = false);
      _scaffoldKey.currentState.showSnackBar(
        SnackBar(content: Text(errors['message'])),
      );
    });
  }
}
