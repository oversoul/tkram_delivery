import 'package:flutter/material.dart';
import 'package:tkram_delivery/app_state.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:tkram_delivery/pages/chat/message_form.dart';
import 'package:tkram_delivery/pages/chat/message_bubble.dart';
import 'package:tkram_delivery/pages/chat/chat_order_dialog.dart';

class MessagesPage extends StatefulWidget {
  final String name;
  final String image;
  final String themId;

  MessagesPage(this.themId, this.name, this.image);

  @override
  _MessagesPageState createState() => _MessagesPageState();
}

class _MessagesPageState extends State<MessagesPage> {
  int meId;
  String chatSlug;

  @override
  void initState() {
    meId = AppState().me$.value.id;
    final themId = int.parse(widget.themId);
    if (meId == themId) {
      throw "This is baaad";
    }

    chatSlug =
        meId > themId ? "user$themId-user$meId" : "user$meId-user$themId";

    print(chatSlug);

    print("=========================");

    initFirebaseDocument();
    super.initState();
  }

  void initFirebaseDocument() async {
    final snapShot =
        await Firestore.instance.collection('chats').document(chatSlug).get();

    if (!snapShot.exists) {
      Firestore.instance.collection('chats').document(chatSlug).setData({});
    }
  }

  Widget _orderAction() {
    if (AppState().me$.value.type != "driver") {
      return Container();
    }

    return FlatButton(
      child: Text("Order"),
      onPressed: () {
        showDialog(
          context: context,
          barrierDismissible: true,
          builder: (_) {
            return WillPopScope(
              onWillPop: () async => true,
              child: OrderDialog(int.parse(widget.themId)),
            );
          },
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: [
            CircleAvatar(
              backgroundImage: widget.image != ''
                  ? NetworkImage('http://tkramdelivery.com/${widget.image}')
                  : null,
            ),
            SizedBox(width: 10),
            Text("${widget.name}"),
          ],
        ),
        actions: [
          _orderAction(),
        ],
      ),
      body: StreamBuilder(
        stream: Firestore.instance
            .collection('chats/$chatSlug/messages')
            .orderBy('createdAt', descending: true)
            .snapshots(),
        builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) {
            return Center(child: Text("Error..."));
          }

          if (!snapshot.hasData) {
            return Center(child: CircularProgressIndicator());
          }

          List<DocumentSnapshot> documents = snapshot.data.documents;

          return Column(
            children: <Widget>[
              Expanded(
                child: ListView.builder(
                  reverse: true,
                  itemBuilder: (context, index) {
                    final message = documents[index];
                    return MessageBubble(
                      key: ValueKey(message.documentID),
                      message: message,
                      slug: chatSlug,
                    );
                  },
                  itemCount: documents.length,
                ),
              ),
              MessageSendForm(chatSlug, widget.themId, widget.name),
            ],
          );
        },
      ),
    );
  }
}
