import 'dart:async';

import 'package:flutter/material.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:photo_view/photo_view.dart';
import 'package:tkram_delivery/app_state.dart';
import 'package:tkram_delivery/blocs/users_bloc.dart';

class MessageBubble extends StatefulWidget {
  final String slug;
  final DocumentSnapshot message;

  MessageBubble({Key key, this.message, this.slug}) : super(key: key);

  @override
  _MessageBubbleState createState() => _MessageBubbleState();
}

class _MessageBubbleState extends State<MessageBubble> {
  Duration _duration;
  Duration _position;
  AudioPlayer audioPlayer;
  AudioPlayerState playerState = AudioPlayerState.STOPPED;

  //
  StreamSubscription _stateSubscription;
  StreamSubscription _durationSubscription;
  StreamSubscription _positionSubscription;
  StreamSubscription _playerCompleteSubscription;

  @override
  initState() {
    audioPlayer = AudioPlayer();
    setupPlayer();
    super.initState();
  }

  void setupPlayer() {
    _durationSubscription = audioPlayer.onDurationChanged.listen((duration) {
      setState(() => _duration = duration);
    });
    _stateSubscription =
        audioPlayer.onPlayerStateChanged.listen((AudioPlayerState s) {
      if (!mounted) return;
      setState(() => playerState = s);
    });

    _playerCompleteSubscription = audioPlayer.onPlayerCompletion
        .listen((_) => setState(() => playerState = AudioPlayerState.STOPPED));
    _positionSubscription = audioPlayer.onAudioPositionChanged
        .listen((p) => setState(() => _position = p));
  }

  @override
  void dispose() {
    audioPlayer.stop().whenComplete(() => audioPlayer.dispose());
    _stateSubscription?.cancel();
    _durationSubscription?.cancel();
    _positionSubscription?.cancel();
    _playerCompleteSubscription?.cancel();
    super.dispose();
  }

  Widget _time() {
    DateTime time = widget.message['createdAt'].toDate();
    return Padding(
      padding: EdgeInsets.only(bottom: 10),
      child: Text(
        "${time.hour}:${time.minute}",
        style: TextStyle(fontSize: 12),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    bool isMe = widget.message['userId'] == AppState().me$.value.id;
    Color textColor = isMe ? Colors.white : Theme.of(context).primaryColor;
    Color background = isMe ? Theme.of(context).primaryColor : Colors.white;

    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: isMe ? MainAxisAlignment.end : MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        if (isMe) _time(),
        Card(
          margin: EdgeInsets.only(bottom: 10, left: 10, right: 10),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(10),
            ),
          ),
          color: background,
          child: Container(
            child: _getValue(textColor),
            padding: EdgeInsets.all(10),
          ),
        ),
        if (!isMe) _time(),
      ],
    );
  }

  Future<void> onPlayAudio(String url) async {
    if (playerState == AudioPlayerState.PLAYING) {
      await audioPlayer.pause();
    } else {
      await audioPlayer.play(url, isLocal: false);
    }

    // audioPlayer.setPlaybackRate(playbackRate: 1.0);
  }

  Widget _getValue(Color color) {
    final isImage = widget.message['type'] == "image";
    final isAudio = widget.message['type'] == "audio";
    final isLoading = widget.message['type'] == "loading";
    final isOrder = widget.message['type'] == "order";

    if (isLoading) {
      return Container(
        child: CircularProgressIndicator(
          backgroundColor: color,
        ),
      );
    }

    if (isOrder) {
      return OrderMessageBubble(widget.message, widget.slug, color);
    }

    if (isAudio) {
      // print(_duration?.inMilliseconds);
      // print(_position?.inMilliseconds);
      return Container(
        width: MediaQuery.of(context).size.width * .8,
        child: Row(
          children: [
            InkWell(
              child: (playerState == AudioPlayerState.STOPPED ||
                      playerState == AudioPlayerState.PAUSED)
                  ? Icon(Icons.play_arrow, color: color)
                  : Icon(Icons.pause, color: color),
              onTap: () async => await onPlayAudio(widget.message['message']),
            ),
            Expanded(
              child: Slider(
                min: 0,
                onChanged: (value) {},
                onChangeEnd: (value) async {
                  await audioPlayer.seek(Duration(milliseconds: value.toInt()));
                },
                activeColor: color,
                inactiveColor: Colors.grey,
                max: _duration?.inMilliseconds?.toDouble() ?? 0,
                value: _position?.inMilliseconds?.toDouble() ?? 0,
              ),
            ),
          ],
        ),
      );
    }

    if (isImage) {
      return Container(
        height: 200,
        width: MediaQuery.of(context).size.width - 80,
        child: GestureDetector(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => HeroPhotoViewWrapper(
                  imageProvider: NetworkImage(widget.message['message']),
                ),
              ),
            );
          },
          child: Hero(
            tag: "image",
            child: Image.network(
              widget.message['message'],
              fit: BoxFit.cover,
            ),
          ),
        ),
      );
    }

    return Text(
      widget.message['message'],
      style: TextStyle(color: color),
    );
  }
}

class HeroPhotoViewWrapper extends StatelessWidget {
  const HeroPhotoViewWrapper({
    this.imageProvider,
    this.loadingBuilder,
    this.backgroundDecoration,
    this.minScale,
    this.maxScale,
  });

  final dynamic maxScale;
  final dynamic minScale;
  final ImageProvider imageProvider;
  final LoadingBuilder loadingBuilder;
  final Decoration backgroundDecoration;

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints.expand(
        height: MediaQuery.of(context).size.height,
      ),
      child: PhotoView(
        minScale: minScale,
        maxScale: maxScale,
        imageProvider: imageProvider,
        loadingBuilder: loadingBuilder,
        backgroundDecoration: backgroundDecoration,
        heroAttributes: PhotoViewHeroAttributes(tag: "image"),
      ),
    );
  }
}

class OrderMessageBubble extends StatefulWidget {
  final Color color;
  final String slug;
  final DocumentSnapshot message;

  OrderMessageBubble(this.message, this.slug, this.color);

  @override
  _OrderMessageBubbleState createState() => _OrderMessageBubbleState();
}

class _OrderMessageBubbleState extends State<OrderMessageBubble> {
  UsersBloc _userBloc;

  @override
  initState() {
    _userBloc = UsersBloc();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 10, left: 10, right: 10),
      width: MediaQuery.of(context).size.width * .8,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Icon(Icons.add_shopping_cart, color: widget.color),
              SizedBox(width: 5),
              Text(
                widget.message['message'],
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: widget.color,
                ),
              ),
            ],
          ),
          SizedBox(height: 10),
          _actions(),
        ],
      ),
    );
  }

  void updateOrder(int value) {
    Firestore.instance
        .collection('chats/${widget.slug}/messages')
        .document(widget.message.documentID)
        .updateData({
      'orderState': value,
    });
  }

  Widget _buttons() {
    if (AppState().me$.value.type == "driver") {
      return FlatButton(child: Text("Waiting..."), onPressed: null);
    }

    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          FlatButton(
            padding: EdgeInsets.all(0),
            color: Colors.white,
            child: Icon(Icons.check),
            onPressed: () async {
              await _userBloc.acceptOrder(widget.message['orderId']);
              updateOrder(1);
            },
          ),
          FlatButton(
            color: Colors.white,
            padding: EdgeInsets.all(0),
            child: Icon(Icons.block, color: Colors.red[300]),
            onPressed: () async {
              await _userBloc.refuseOrder(widget.message['orderId']);
              updateOrder(0);
            },
          ),
        ],
      ),
    );
  }

  Widget _actions() {
    if (widget.message['orderState'] == -1) {
      return _buttons();
    }

    if (widget.message['orderState'] == 1) {
      return FlatButton(child: Text("Accepted"), onPressed: null);
    }

    if (widget.message['orderState'] == 0) {
      return FlatButton(child: Text("Refused"), onPressed: null);
    }

    return Container();
  }
}
