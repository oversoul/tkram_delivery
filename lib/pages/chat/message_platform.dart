import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:tkram_delivery/app_state.dart';
import 'package:tkram_delivery/models/user_model.dart';
import 'package:tkram_delivery/providers/notifications_provider.dart';

enum MessageType {
  Text,
  Image,
  Audio,
  Order,
  Loading,
}

extension ParseValue on MessageType {
  String get value {
    return this.toString().toLowerCase().split('.').last;
  }
}

class MessagePlatform {
  Profile me;
  String themId;
  String avatar;
  String chatUrl;
  NotificationsProvider _notifications;

  MessagePlatform(String themId) {
    this.themId = themId;
    this.me = AppState().me$.value;
    this.avatar = AppState().prefs.getString('avatar');

    chatUrl = me.id > int.parse(themId)
        ? "user$themId-user${me.id}"
        : "user${me.id}-user$themId";

    _notifications = NotificationsProvider();
  }

  dispose() {
    _notifications.dispose();
  }

  Future<DocumentReference> sendMessage(MessageType type, String value,
      [int orderId = 0]) async {
    Map<String, dynamic> chatMessage = {
      'createdAt': Timestamp.now(),
      'message': value,
      'userId': me.id,
      'type': type.value,
      'orderId': orderId,
    };

    if (orderId > 0) {
      chatMessage['orderState'] = -1; 
    }

    await _notifications.send(me.id, me.name, avatar, value ?? type.value, themId);

    return Firestore.instance
        .collection('chats/$chatUrl/messages')
        .add(chatMessage);
  }
}
