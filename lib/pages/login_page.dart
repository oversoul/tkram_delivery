import 'package:flutter/material.dart';
import 'package:tkram_delivery/app_state.dart';
import 'package:tkram_delivery/blocs/auth_bloc.dart';
import 'package:tkram_delivery/helpers.dart';
import 'package:tkram_delivery/pages/widgets/custom_input.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  AuthBloc authBloc;
  final _formKey = GlobalKey<FormState>();
  final globalKey = GlobalKey<ScaffoldState>();
  Map<String, String> credentials = Map<String, String>();

  @override
  void initState() {
    super.initState();
    authBloc = AuthBloc();
  }

  @override
  void dispose() {
    authBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: globalKey,
      bottomNavigationBar: _signupButton(context),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            children: <Widget>[
              // Container(
              //   alignment: Alignment.centerRight,
              //   child: Switch(
              //     value: false,
              //     onChanged: (value) {},
              //   ),
              // ),
              Container(
                width: 120,
                child: Image.asset("assets/tekram.png"),
              ),
              SizedBox(height: 20),
              _title(context),
              SizedBox(height: 50),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 40, vertical: 20),
                child: Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      CustomInput(
                        label: "Phone number",
                        icon: Icons.phone,
                        validator: (value) {
                          if (value.isEmpty) {
                            return "Please fill in your phone number";
                          }
                          return null;
                        },
                        onSaved: (value) => credentials['phone'] = value,
                      ),
                      CustomInput(
                        label: "Password",
                        icon: Icons.lock,
                        isPassword: true,
                        validator: (value) {
                          if (value.isEmpty) {
                            return "Please fill in your password";
                          }
                          return null;
                        },
                        onSaved: (value) => credentials['password'] = value,
                      ),
                      FlatButton(
                        onPressed: _attemptLogin,
                        child: Text("Log in"),
                        color: Theme.of(context).primaryColor,
                      ),
                      FlatButton(
                        child: Text("Verify phone"),
                        onPressed: () {
                          AppState().rootNavigator.currentState.pushNamed("/auth/confirm");
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _snackBar(String text) {
    globalKey.currentState.showSnackBar(
      SnackBar(content: Text(text)),
    );
  }

  void _attemptLogin() async {
    if (!_formKey.currentState.validate()) {
      _snackBar('Please make sure to fill the form correctly.');
      return;
    }

    AppState().globalLoader.show();
    await dismissKeyboard(context);
    _formKey.currentState.save();

    final phone = credentials['phone'];
    final password = credentials['password'];

    authBloc.login(phone, password).then((bool value) {
      AppState().globalLoader.hide();
      _snackBar("Couldn't login");
      appState.evtAuthState$.add(value);
    }).catchError((errors) {
      AppState().globalLoader.hide();
      _snackBar(errors['message']);
    });
  }

  InkWell _signupButton(BuildContext context) {
    return InkWell(
      onTap: () {
        AppState().rootNavigator.currentState.pushNamed("/signup");
      },
      child: Container(
        width: double.infinity,
        padding: EdgeInsets.all(15),
        color: Theme.of(context).primaryColor,
        child: RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            style: TextStyle(fontWeight: FontWeight.w500),
            text: "Don't have an account ",
            children: <TextSpan>[
              TextSpan(
                text: "Sign Up",
                style: TextStyle(
                  color: Color(0xffEEAD1F),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _title(BuildContext context) {
    return RichText(
      text: TextSpan(
        text: 'Welcome to ',
        style: TextStyle(
          fontSize: 28,
          color: Color(0xff7F7F7F),
          fontWeight: FontWeight.w500,
        ),
        children: <TextSpan>[
          TextSpan(
            text: 'TKRAM',
            style: TextStyle(
              color: Theme.of(context).primaryColor,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }
}
