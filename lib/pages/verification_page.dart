import 'package:flutter/material.dart';
import 'package:tkram_delivery/app_state.dart';
import 'package:tkram_delivery/blocs/auth_bloc.dart';
import 'package:tkram_delivery/pages/widgets/custom_input.dart';

class VerificationModel {
  String phoneNumber;
  String verificationCode;
}

class VerificationPage extends StatefulWidget {
  @override
  _VerificationPageState createState() => _VerificationPageState();
}

class _VerificationPageState extends State<VerificationPage> {
  final _formKey = GlobalKey<FormState>();
  VerificationModel _model = VerificationModel();
  AuthBloc authBloc;

  @override
  void initState() {
    authBloc = AuthBloc();
    super.initState();
  }

  @override
  void dispose() {
    authBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Verification"),
      ),
      body: SafeArea(
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(left: 20, right: 20, top: 20),
                child: CustomInput(
                  label: "Phone Number",
                  icon: Icons.phone,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please enter valid phone number';
                    }
                    return null;
                  },
                  onSaved: (value) => _model.phoneNumber = value,
                ),
              ),
              VerificationInput(
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter verification code';
                  }
                  return null;
                },
                onSaved: (value) => _model.verificationCode = value,
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 20),
                width: double.infinity,
                child: FlatButton(
                  color: Theme.of(context).primaryColor,
                  child: Text("Verify"),
                  onPressed: _verify,
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 20),
                width: double.infinity,
                child: FlatButton(
                  child: Text("Resend Code"),
                  onPressed: _resend,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _verify() {
    final snackBar = SnackBar(
      content: Text('Please make sure to fill the form correctly.'),
    );
    if (!_formKey.currentState.validate()) {
      Scaffold.of(context).showSnackBar(snackBar);
      return;
    }
    
    _formKey.currentState.save();
    AppState().globalLoader.show();

    authBloc.verify(_model.phoneNumber, _model.verificationCode).then((value) {
      AppState().globalLoader.hide();
      AppState().rootNavigator.currentState.pop();
    }).catchError((errors) {
      AppState().globalLoader.hide();
      print(errors);
      Scaffold.of(context).showSnackBar(
        SnackBar(content: Text(errors['message'])),
      );
      return;
    });
  }

  void _resend() {
    _formKey.currentState.save();
    final snackBar = SnackBar(
      content: Text('Please input your phone number.'),
    );

    if (_model.phoneNumber.isEmpty) {
      Scaffold.of(context).showSnackBar(snackBar);
      return;
    }

    AppState().globalLoader.show();

    authBloc.resend(_model.phoneNumber).then((value) {
      AppState().globalLoader.hide();
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text('Verification code sent.'),
      ));
    }).catchError((errors) {
      AppState().globalLoader.hide();
      print(errors);
      Scaffold.of(context).showSnackBar(
        SnackBar(content: Text(errors['message'])),
      );
      return;
    });
  }
}

class VerificationInput extends StatefulWidget {
  final void Function(String) onSaved;
  final String Function(String) validator;

  VerificationInput({this.validator, this.onSaved});

  @override
  _VerificationInputState createState() => _VerificationInputState();
}

class _VerificationInputState extends State<VerificationInput> {
  OutlineInputBorder inputBorder() {
    return OutlineInputBorder(
      borderRadius: BorderRadius.all(
        Radius.circular(30),
      ),
      borderSide: BorderSide(
        width: 1,
        color: Color(0xffD6D6D6),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final _border = inputBorder();
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: TextFormField(
        maxLength: 6,
        style: TextStyle(fontSize: 24),
        textAlign: TextAlign.center,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          hintText: "_ _ _ _ _ _",
          contentPadding: EdgeInsets.zero,
          disabledBorder: _border,
          enabledBorder: _border,
          focusedBorder: _border,
          errorBorder: _border,
          border: _border,
          filled: true,
        ),
        onSaved: widget.onSaved,
        validator: widget.validator,
      ),
    );
  }
}
