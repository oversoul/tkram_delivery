import 'package:flutter/widgets.dart';

/// dismiss keyboard with delay
Future<void> dismissKeyboard(
  BuildContext context, {
  Duration delay: const Duration(milliseconds: 100),
}) async {
  // manually dismiss the keyboard
  FocusScope.of(context).requestFocus(new FocusNode());

  // a slight delay until the keyboard is dismissed
  await Future.delayed(delay);
}


/*
  try {
    AuthResult authState = await FirebaseAuth.instance.signInAnonymously();  

    // FirebaseAuth.instance.signOut();            

  } on PlatformException catch (e) {
    print(e);
  }
*/