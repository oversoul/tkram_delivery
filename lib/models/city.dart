class City {
  int id;
  String name;

  City({this.id, this.name});

  static fromJson(Map<String, dynamic> json) {
    return City(
      id: json['id'] as int,
      name: json['name'],
    );
  }
}

class CityList {
  List<City> data;

  CityList({this.data});

  static fromJson(Map<String, dynamic> json) {
    var cities = List<City>();

    for (var city in json['cities']) {
      cities.add(City.fromJson(city));
    }

    return CityList(data: cities);
  }
}
