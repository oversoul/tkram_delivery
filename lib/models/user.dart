part 'user.g.dart';

class User {
  int id;
  String name;
  String city;
  String image;
  double fullAddress;
  String phoneNumber;

  // "created_at": "01-April-2020",
  // "updated_at": "01-April-2020"

  User({id, name, city, fullAddress, phoneNumber, image})
      : this.id = id ?? 0,
        this.name = name ?? "",
        this.city = city ?? "",
        this.image = image ?? "",
        this.phoneNumber = phoneNumber ?? "",
        this.fullAddress = fullAddress ?? 0;

  static User fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
}

class UserList {
  final List<User> data;

  UserList(this.data);

  static UserList fromJson(Map<String, dynamic> json) =>
      _$UserListFromJson(json);
}

class CustomUsers {
  final Map<int, User> data;

  CustomUsers(this.data);

  static CustomUsers fromJson(Map<String, dynamic> json) =>
      _$CustomUserListFromJson(json);
}
