part of 'statement.dart';

Statement _$StatementFromJson(Map<String, dynamic> json) {
  return Statement(
    id: json['id'],
    clientName: json['client_name'],
    userId: int.parse(json['user_id']),
    price: double.parse(json['price']),
    driverId: int.parse(json['driver_id']),
  );
}

StatementList _$StatementListFromJson(Map<String, dynamic> json) {
  final statements = List<Statement>();

  for (var order in json['orders']) {
    statements.add(_$StatementFromJson(order));
  }

  return StatementList(statements);
}
