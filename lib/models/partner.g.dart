part of 'partner.dart';

Partner _$PartnerFromJson(Map<String, dynamic> partner) {
  return Partner(
    id: partner['id'],
    name: partner['name'],
    logoUrl: partner['logo_url'],
    facebook: partner['facebook'],
    instagram: partner['instagram'],
    phoneNumber: partner['phone_number'],
    descriptionAr: partner['description_ar'],
    descriptionFr: partner['description_fr'],
    descriptionEn: partner['description_en'],
  );
}

PartnerList _$PartnerListFromJson(Map<String, dynamic> json) {
  List<Partner> partners = [];
  for (var partner in json['partners']) {
    partners.add(_$PartnerFromJson(partner));
  }

  return PartnerList(partners);
}
