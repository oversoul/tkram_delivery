class RegistrationModel {
  String name;
  String phone;
  String city;
  String address;
  String password;
  String confirmationPassword;

  RegistrationModel({
    this.name,
    this.phone,
    this.city,
    this.address,
    this.password,
    this.confirmationPassword,
  });

  Map<String, String> toForm() {
    return {
      "name": name,
      "city": city,
      "password": password,
      "phone_number": phone,
      "full_address": address,
      "password_confirmation": confirmationPassword
    };
  }
}
