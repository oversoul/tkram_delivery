part of 'price.dart';

Price _$PriceFromJson(Map<String, dynamic> data) {
  return Price(
    id: data['id'] as int,
    to: data['to'],
    from: data['from'],
    price: double.parse(data['price']),
  );
}

PriceList _$PriceListFromJson(Map<String, dynamic> json) {
  List<Price> prices = List<Price>();

  for (var price in json['prices']) {
    prices.add(_$PriceFromJson(price));
  }

  return PriceList(data: prices);
}
