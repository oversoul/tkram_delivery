part 'price.g.dart';

class Price {
  int id;
  String to;
  String from;
  double price;
  // "created_at": "2020-04-02 08:15:51",
  // "updated_at": "2020-04-02 08:15:51"

  Price({id, from, to, price})
      : this.id = id ?? 0,
        this.from = from ?? "",
        this.to = to ?? "",
        this.price = price ?? 0;

  static Price fromJson(Map<String, dynamic> json) => _$PriceFromJson(json);
}

class PriceList {
  List<Price> data;

  PriceList({data}) : this.data = data ?? [];

  static PriceList fromJson(Map<String, dynamic> json) =>
      _$PriceListFromJson(json);
}
