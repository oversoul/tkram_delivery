part of 'driver.dart';

Driver _$DriverFromJson(Map<String, dynamic> json) {
  // driver: {id: 3, user_id: 21, avatar_url: /storage/96/12389.png, description: srgrdfgdfgg, image_url: /storage/77/vespa111.jpeg, plate_number: 1234, avaliable: 1, s1: 0, s2: 0, s3: 0, s4: 0, s5: 0, created_at: 31-March-2020, updated_at: 16-April-2020}
  final driver = json['driver'];
  final isDriver = driver != null;

  return Driver(
    id: json['id'],
    name: json['name'],
    city: json['city'] ?? "",
    imageUrl: isDriver ? driver['image_url'] : "",
    avatarUrl: isDriver ? driver['avatar_url'] : "",
    userId: json['userId'] as int,
    active: json['active'] == "1",
    phoneNumber: json['phoneNumber'],
    fullAddress: json['fullAddress'],
    description: isDriver ? driver['description'] : "",
    plateNumber: isDriver ? driver['plateNumber'] : "",
    available: isDriver ? driver['avaliable'] == "1" : false,
    joinedSince: isDriver ? driver['created_at'] : "",
  );
}

DriverList _$DriverListFromJson(Map<String, dynamic> json) {
  List<Driver> busyDrivers = [];
  List<Driver> availableDrivers = [];
  for (var driver in json['drivers']) {
    var driverM = _$DriverFromJson(driver);
    if (driverM.available) {
      availableDrivers.add(driverM);
    } else {
      busyDrivers.add(driverM);
    }
  }

  return DriverList(
    available: availableDrivers,
    busy: busyDrivers,
  );
}

SearchDrivers _$DriverSearchListFromJson(Map<String, dynamic> json) {
  List<Driver> drivers = List<Driver>();
  for (var driver in json['drivers']) {
    drivers.add(_$DriverFromJson(driver));
  }

  return SearchDrivers(drivers);
}

FavDrivers _$FavDriversFromJson(Map<String, dynamic> json) {
  final users = json['fav_drivers'];
  List<Driver> drivers = List<Driver>();

  for (var user in users) {
    drivers.add(_$DriverFromJson(user['driver']));
  }

  return FavDrivers(drivers);
}