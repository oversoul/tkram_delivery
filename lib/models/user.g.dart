part of 'user.dart';

User _$UserFromJson(Map<String, dynamic> json) {
  return User(
    id: json['id'],
    name: json['name'],
    city: json['city'],
    image: json['image'] ?? null,
    phoneNumber: json['phoneNumber'],
    fullAddress: json['fullAddress'],
  );
}

UserList _$UserListFromJson(Map<String, dynamic> json) {
  List<User> users = List<User>();
  for (var user in json['users']) {
    users.add(User.fromJson(user));
  }

  return UserList(users);
}

CustomUsers _$CustomUserListFromJson(Map<String, dynamic> json) {
  Map<int, User> users = Map<int, User>();
  for (var user in json['users']) {
    var userObj = User.fromJson(user);
    users[userObj.id] = userObj;
  }

  return CustomUsers(users);
}
