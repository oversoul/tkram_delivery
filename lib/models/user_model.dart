class UserModel {
  UserType type;
  bool isVerified;
}

class UserState {
  UserType type;
  String accessToken;

  UserState(this.type, this.accessToken);

  factory UserState.fromJson(Map<dynamic, dynamic> data) {
    final accessToken = data['access_token'];
    final type =
        data['user_type'] == "driver" ? UserType.Driver : UserType.Client;
    return UserState(type, accessToken);
  }
}

enum UserType { Driver, Client }

class Profile {
  // {"id":67,"name":"amine","phone_number":"999999999","phone_verified_at":"2020-05-12 00:12:15","city":"Tripolitania","full_address":"somewhere","active":"1","type":"user","created_at":"12-May-2020","updated_at":"12-May-2020","roles":[{"id":3,"name":"user","guard_name":"web","created_at":"2020-03-30 17:00:00","updated_at":"2020-03-30 17:00:00","pivot":{"model_id":"67","role_id":"3"}}]}
  int id;
  String name;
  String phoneNumber;
  String city;
  String fullAddress;
  bool active;
  String type;

  Profile({id, name, phoneNumber, city, fullAddress, active, type})
      : this.id = id ?? 0,
        this.name = name ?? "",
        this.city = city ?? "",
        this.type = type ?? "",
        this.active = active ?? false,
        this.phoneNumber = phoneNumber ?? "",
        this.fullAddress = fullAddress ?? "";

  static Profile fromJson(Map<String, dynamic> json) {
    final user = json['user'];
    return Profile(
      id: user['id'] as int,
      name: user['name'],
      type: user['type'],
      city: user['city'],
      active: user['active'] == "1",
      phoneNumber: user['phone_number'],
      fullAddress: user['full_address'],
    );
  }
}
