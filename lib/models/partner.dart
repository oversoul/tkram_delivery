part 'partner.g.dart';

class Partner {
  int id;
  String name;
  String logoUrl;
  String facebook;
  String instagram;
  String phoneNumber;
  String descriptionAr;
  String descriptionEn;
  String descriptionFr;

  Partner({
    id,
    name,
    logoUrl,
    facebook,
    instagram,
    phoneNumber,
    descriptionAr,
    descriptionEn,
    descriptionFr,
  })  : this.id = id ?? 0,
        this.name = name ?? "",
        this.logoUrl =
            logoUrl != null ? "https://tkramdelivery.com/$logoUrl" : '',
        this.facebook = facebook ?? null,
        this.instagram = instagram ?? null,
        this.phoneNumber = phoneNumber ?? "",
        this.descriptionAr = descriptionAr ?? "",
        this.descriptionFr = descriptionFr ?? "",
        this.descriptionEn = descriptionEn ?? "";

  static Partner fromJson(Map<String, dynamic> json) => _$PartnerFromJson(json);
}

class PartnerList {
  final List<Partner> all;

  PartnerList(this.all);

  static PartnerList fromJson(Map<String, dynamic> json) =>
      _$PartnerListFromJson(json);
}
