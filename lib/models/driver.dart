part 'driver.g.dart';

class Driver {
  int id;
  String name;
  String phoneNumber;
  String city;
  String fullAddress;
  bool active;
  int userId;
  String avatarUrl;
  String description;
  String imageUrl;
  String plateNumber;
  bool available;
  String joinedSince;

  // "created_at": "31-March-2020",
  // "updated_at": "16-April-2020",
  // "phone_verified_at": "2020-03-31 15:42:41",
  // "s1": "0",
  // "s2": "0",
  // "s3": "0",
  // "s4": "0",
  // "s5": "0"

  Driver({
    id,
    name,
    phoneNumber,
    city,
    fullAddress,
    active,
    userId,
    avatarUrl,
    description,
    imageUrl,
    plateNumber,
    available,
    joinedSince,
  })  : this.id = id ?? 0,
        this.name = name ?? "",
        this.city = city ?? "",
        this.userId = userId ?? 0,
        this.active = active ?? false,
        this.imageUrl = imageUrl ?? null,
        this.avatarUrl = avatarUrl ?? null,
        this.available = available ?? false,
        this.phoneNumber = phoneNumber ?? "",
        this.fullAddress = fullAddress ?? "",
        this.description = description ?? "",
        this.plateNumber = plateNumber ?? "",
        this.joinedSince = joinedSince ?? "";

  static Driver fromJson(Map<String, dynamic> json) => _$DriverFromJson(json);
}

class DriverList {
  final List<Driver> busy;
  final List<Driver> available;

  DriverList({this.available, this.busy});

  static DriverList fromJson(Map<String, dynamic> json) =>
      _$DriverListFromJson(json);

  // Map<String, dynamic> toJson() => _$DriverListToJson(this);
}

class SearchDrivers {
  final List<Driver> data;

  SearchDrivers(this.data);

  static SearchDrivers fromJson(Map<String, dynamic> json) =>
      _$DriverSearchListFromJson(json);
}


class FavDrivers {
  final List<Driver> data;

  FavDrivers(this.data);
  static FavDrivers fromJson(Map<String, dynamic> json) => _$FavDriversFromJson(json);
}