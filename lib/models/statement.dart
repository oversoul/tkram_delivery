part 'statement.g.dart';

class Statement {
  int id;
  int userId;
  int driverId;
  String clientName;
  double price;

  Statement({
    id,
    userId,
    driverId,
    clientName,
    price,
  })  : this.id = id ?? 0,
        this.price = price ?? 0,
        this.userId = userId ?? 0,
        this.driverId = driverId ?? 0,
        this.clientName = clientName ?? "";


  static Statement fromJson(Map<String, dynamic> json) =>
      _$StatementFromJson(json);
}

class StatementList {
  List<Statement> statements;

  StatementList(statements) : this.statements = statements ?? [];

  static StatementList fromJson(Map<String, dynamic> json) =>
      _$StatementListFromJson(json);
}