import 'dart:async';
import 'package:tkram_delivery/models/city.dart';
import 'package:tkram_delivery/models/user.dart';
import 'package:tkram_delivery/models/price.dart';
import 'package:tkram_delivery/models/driver.dart';
import 'package:tkram_delivery/models/partner.dart';
import 'package:tkram_delivery/models/statement.dart';
import 'package:tkram_delivery/providers/auth_provider.dart';
import 'package:tkram_delivery/providers/users_provider.dart';
import 'package:tkram_delivery/models/registration_model.dart';
import 'package:tkram_delivery/providers/cities_provider.dart';
import 'package:tkram_delivery/providers/prices_provider.dart';
import 'package:tkram_delivery/providers/drivers_provider.dart';
import 'package:tkram_delivery/providers/partners_provider.dart';
import 'package:tkram_delivery/providers/contactus_provider.dart';
import 'package:tkram_delivery/providers/statements_provider.dart';

class Repository {
  final authProvider = AuthProvider();
  final userProvider = UsersProvider();
  final pricesProvider = PricesProvider();
  final citiesProvider = CitiesProvider();
  final driversProvider = DriversProvider();
  final partnersProvider = PartnersProvider();
  final contactUsProvider = ContactUsProvider();
  final statementsProvider = StatementsProvider();

  Future<bool> sendContactMessage(String name, String email, String message) =>
      contactUsProvider.send(name, email, message);

  Future<bool> login(String phone, String password) =>
      authProvider.login(phone, password);

  Future<bool> register(RegistrationModel model) =>
      authProvider.register(model);
  Future<bool> verify(String phone, String code) =>
      authProvider.verify(phone, code);

  Future<bool> resend(String phone) => authProvider.resend(phone);

  Future<bool> isAvailable() => authProvider.isAvailable();
  Future<bool> setAvailable(bool value) => authProvider.setAvailable(value);

  Future<DriverList> getAllDrivers() => driversProvider.getAll();
  Future<SearchDrivers> searchDrivers(String keyword) =>
      driversProvider.search(keyword);
  Future<FavDrivers> getFavoriteDrivers() => driversProvider.getFavorites();
  Future<Driver> getDriverById(int id) => driversProvider.getById(id);
  Future<bool> isDriverFavorite(int id) => driversProvider.isFavorite(id);
  Future<void> setFavoriteStatus(int id, bool state) =>
      driversProvider.setFavorite(id, state);

  Future<int> createOrder(int clientId, double price) =>
      driversProvider.createOrder(clientId, price);

  Future<bool> acceptOrder(int orderId) => userProvider.acceptOrder(orderId);
  Future<bool> refuseOrder(int orderId) => userProvider.refuseOrder(orderId);

  Future<PriceList> getAllPrices() => pricesProvider.getAll();

  Future<UserList> getAllUsers() => userProvider.getAll();
  Future<CustomUsers> getUsersByIds(String ids) => userProvider.getByIds(ids);

  Future<StatementList> getStatements() => statementsProvider.getAll();

  Future<void> isUserTokenValid() => authProvider.isValid();

  Future<PartnerList> partners() => partnersProvider.getAll();

  Future<CityList> cities() => citiesProvider.getAll();

  Future<bool> uploadImage(String path) => userProvider.uploadImage(path);

  dispose() {
    userProvider.dispose();
    authProvider.dispose();
    pricesProvider.dispose();
    driversProvider.dispose();
    partnersProvider.dispose();
    contactUsProvider.dispose();
    statementsProvider.dispose();
  }
}
