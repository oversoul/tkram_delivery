import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' show Client;
import 'package:tkram_delivery/app_state.dart';
import 'package:tkram_delivery/models/registration_model.dart';

class AuthProvider {
  Client client = Client();

  Future<void> isValid() async {
    final token = AppState().prefs.getString("token");
    final expiration = AppState().prefs.getString("expiration");

    if (token == null || expiration == null) {
      AppState().logout();
      return;
    }

    final date = DateTime.parse(expiration);

    if (date.isBefore(DateTime.now())) {
      AppState().logout();
    }
  }

  Future<bool> login(String phoneNumber, String password) async {
    final response = await client.post(
      "http://tkramdelivery.com/api/auth/login",
      body: {
        'phone_number': phoneNumber,
        'password': password,
        'remember_me': '0',
      },
      headers: {"Accept": "application/json"},
    );

    if (response.statusCode == 200) {
      final body = json.decode(response.body);

      await AppState().prefs.setString("user_type", body['user_type']);
      await AppState().prefs.setString("token", body['access_token']);
      await AppState().prefs.setString("expiration", body['expires_at']);
      await AppState().prefs.setString('avatar', body['image']);
      return true;
    }

    if (response.statusCode == 401 || response.statusCode == 422) {
      return Future.error(json.decode(response.body));
    }

    print(response.statusCode);

    return Future.error(json.decode(response.body));
  }

  Future<bool> register(RegistrationModel model) async {
    final response = await client.post(
      "http://tkramdelivery.com/api/auth/signup",
      body: model.toForm(),
      headers: {"Accept": "application/json"},
    );

    if (response.statusCode < 300) {
      return true;
    }

    if (response.statusCode == 422) {
      return Future.error(json.decode(response.body));
    }
    // +212659823239 / adminadmin
    return Future.error('Something went wrong!');
  }

  Future<bool> isAvailable() async {
    final token = AppState().prefs.getString("token");

    if (token == null) {
      return Future.error("Token was not found.");
    }

    final response = await client.get(
      "http://tkramdelivery.com/api/driver/status",
      headers: {
        "Accept": "application/json",
        "Authorization": "Bearer $token",
      },
    );

    if (response.statusCode == 200) {
      Map<String, dynamic> body = json.decode(response.body);
      final status = int.parse(body['status']);
      return status == 1;
    }

    return Future.error(json.decode(response.body));
  }

  Future<bool> setAvailable(bool value) async {
    final token = AppState().prefs.getString("token");

    if (token == null) {
      return Future.error("Token was not found.");
    }

    final status = value ? "1" : "0";

    final response = await client.get(
      "http://tkramdelivery.com/api/driver/status/$status",
      headers: {
        "Accept": "application/json",
        "Authorization": "Bearer $token",
      },
    );

    if (response.statusCode == 200) {
      Map<String, dynamic> body = json.decode(response.body);
      final status = body['status'];
      if (status == 1) {
        return value;
      }
    }

    return Future.error(json.decode(response.body));
  }

  dispose() {
    client.close();
  }

  Future<bool> verify(String phone, String code) async {
    final response = await client.post(
      "http://tkramdelivery.com/api/auth/verify",
      body: {
        'phone_number': phone,
        'verification_code': code,
      },
      headers: {"Accept": "application/json"},
    );

    if (response.statusCode < 300) {
      return true;
    }

    return Future.error(json.decode(response.body));
  }

  Future<bool> resend(String phone) async {
    final response = await client.post(
      "http://tkramdelivery.com/api/auth/resend-code",
      body: {'phone_number': phone, 'operation': 'verify'},
      headers: {"Accept": "application/json"},
    );

    if (response.statusCode < 300) {
      return true;
    }

    return Future.error(json.decode(response.body));
  }
}
