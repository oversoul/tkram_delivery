import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' show Client;
import 'package:tkram_delivery/app_state.dart';
import 'package:tkram_delivery/models/driver.dart';

class DriversProvider {
  Client client = Client();

  Future<DriverList> getAll() async {
    final token = AppState().prefs.getString("token");

    if (token == null) {
      return Future.error("Token was not found.");
    }

    final response = await client.get(
      "http://tkramdelivery.com/api/drivers",
      headers: {"Accept": "application/json", "Authorization": "Bearer $token"},
    );

    if (response.statusCode == 200) {
      return DriverList.fromJson(json.decode(response.body));
    }

    if (response.statusCode == 401) {}

    throw (json.decode(response.body));
  }

  Future<SearchDrivers> search(String keyword) async {
    if (keyword == null) {
      return SearchDrivers([]);
    }

    final token = AppState().prefs.getString("token");

    if (token == null) {
      return Future.error("Token was not found.");
    }

    final response = await client.get(
      "http://tkramdelivery.com/api/drivers?search=$keyword",
      headers: {"Accept": "application/json", "Authorization": "Bearer $token"},
    );

    if (response.statusCode == 200) {
      // await Future.delayed(Duration(seconds: 1));
      return SearchDrivers.fromJson(json.decode(response.body));
    }

    return Future.error(json.decode(response.body));
  }

  Future<FavDrivers> getFavorites() async {
    final token = AppState().prefs.getString("token");

    if (token == null) {
      return Future.error("Token was not found.");
    }

    final response = await client.get(
      "http://tkramdelivery.com/api/user/fav-drivers",
      headers: {
        "Accept": "application/json",
        "Authorization": "Bearer $token",
      },
    );

    if (response.statusCode == 200) {
      final body = json.decode(response.body);
      return FavDrivers.fromJson(body);
    }

    return Future.error(json.decode(response.body));
  }

  Future<Driver> getById(int id) async {
    final token = AppState().prefs.getString("token");

    if (token == null) {
      return Future.error("Token was not found.");
    }

    final response = await client.get(
      "http://tkramdelivery.com/api/driver/$id",
      headers: {
        "Accept": "application/json",
        "Authorization": "Bearer $token",
      },
    );

    if (response.statusCode == 200) {
      final body = json.decode(response.body);
      if (body['status'] == 1) {
        return Driver.fromJson(body['user']);
      }
    }

    return Future.error(json.decode(response.body));
  }

  Future<bool> isFavorite(int id) async {
    final token = AppState().prefs.getString("token");

    if (token == null) {
      return Future.error("Token was not found.");
    }

    final response = await client.get(
      "http://tkramdelivery.com/api/user/fav-drivers/$id",
      headers: {
        "Accept": "application/json",
        "Authorization": "Bearer $token",
      },
    );

    if (response.statusCode == 200) {
      final body = json.decode(response.body);
      return body['status'] == 1;
    }

    return Future.error(json.decode(response.body));
  }

  Future<void> setFavorite(int id, bool state) async {
    final token = AppState().prefs.getString("token");

    if (token == null) {
      return Future.error("Token was not found.");
    }

    String uri = state ? "set-fav" : "unset-fav";

    final response = await client.get(
      "http://tkramdelivery.com/api/user/$uri/$id",
      headers: {
        "Accept": "application/json",
        "Authorization": "Bearer $token",
      },
    );

    if (response.statusCode == 200) {
      // final body = json.decode(response.body);
      return;
    }

    return Future.error(json.decode(response.body));
  }

  Future<int> createOrder(int clientId, double price) async {
    final token = AppState().prefs.getString("token");
    final driverId = AppState().me$.value.id;

    if (token == null || driverId == null) {
      return Future.error("Token|driver was not found.");
    }

    final response = await client.post(
      "http://tkramdelivery.com/api/order/create",
      body: {
        'price': '$price',
        'user_id': '$clientId',
        'driver_id': '$driverId',
      },
      headers: {
        "Accept": "application/json",
        "Authorization": "Bearer $token",
      },
    );

    if (response.statusCode < 300) {
      final body = json.decode(response.body);
      return body['order_id'] as int;
    }

    return Future.error(json.decode(response.body));
  }

  dispose() {
    client.close();
  }
}
