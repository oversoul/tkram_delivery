import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' show Client;
import 'package:tkram_delivery/app_state.dart';
import 'package:tkram_delivery/models/user_model.dart';

class ProfileProvider {
  Client client = Client();

  Future<void> load() async {
    final token = AppState().prefs.getString("token");
    final type = AppState().prefs.getString("user_type");

    if (token == null || type == null) {
      return Future.error("Token/Type was not found.");
    }

    final response = await client.get(
      "http://tkramdelivery.com/api/$type",
      headers: {
        "Accept": "application/json",
        "Authorization": "Bearer $token",
      },
    );

    if (response.statusCode == 200) {
      final profile = Profile.fromJson(json.decode(response.body));
      AppState().me$.sink.add(profile);
      return;
    }

    AppState().me$.sink.addError(json.decode(response.body));
  }

  dispose() {
    client.close();
  }
}
