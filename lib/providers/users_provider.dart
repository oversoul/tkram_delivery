import 'dart:async';
import 'dart:convert';
import 'package:tkram_delivery/app_state.dart';
import 'package:tkram_delivery/models/user.dart';
import 'package:http/http.dart' show Client, MultipartFile, MultipartRequest;

class UsersProvider {
  Client client = Client();

  Future<UserList> getAll() async {
    final token = AppState().prefs.getString("token");

    if (token == null) {
      return Future.error("Token was not found.");
    }

    final response = await client.get(
      "http://tkramdelivery.com/api/users",
      headers: {
        "Accept": "application/json",
        "Authorization": "Bearer $token",
      },
    );

    if (response.statusCode == 200) {
      return UserList.fromJson(json.decode(response.body));
    }

    return Future.error(json.decode(response.body));
  }

  Future<CustomUsers> getByIds(String ids) async {
    final token = AppState().prefs.getString("token");

    if (token == null) {
      return Future.error("Token was not found.");
    }

    final response = await client
        .post("http://tkramdelivery.com/api/driver/get-by-ids", headers: {
      "Accept": "application/json",
      "Authorization": "Bearer $token",
    }, body: {
      'users_ids': ids,
    });

    if (response.statusCode == 200) {
      return CustomUsers.fromJson(json.decode(response.body));
    }

    return Future.error(json.decode(response.body));
  }

  Future<bool> refuseOrder(int orderId) async {
    final token = AppState().prefs.getString("token");
    final driverId = AppState().me$.value.id;

    if (token == null || driverId == null) {
      return Future.error("Token|driver was not found.");
    }

    final response = await client.post(
      "http://tkramdelivery.com/api/user/orders/$orderId/deny",
      body: {},
      headers: {"Accept": "application/json", "Authorization": "Bearer $token"},
    );

    if (response.statusCode == 200) {
      return true;
    }

    return false;
  }

  Future<bool> acceptOrder(int orderId) async {
    final token = AppState().prefs.getString("token");
    final driverId = AppState().me$.value.id;

    if (token == null || driverId == null) {
      return Future.error("Token|driver was not found.");
    }

    final response = await client.post(
      "http://tkramdelivery.com/api/user/orders/$orderId/accept",
      body: {},
      headers: {"Accept": "application/json", "Authorization": "Bearer $token"},
    );

    if (response.statusCode == 200) {
      return true;
    }

    return false;
  }

  Future<bool> uploadImage(String path) async {
    final token = AppState().prefs.getString("token");

    if (token == null) {
      return Future.error("Token was not found.");
    }

    var url = "http://tkramdelivery.com/api/user/picture";
    var request = MultipartRequest('POST', Uri.parse(url));
    request.headers['Authorization'] = "Bearer $token";
    request.headers['Accept'] = "application/json";

    request.files.add(
      await MultipartFile.fromPath('image', path),
    );

    var response = await request.send();
    // await response.stream.bytesToString();

    return response.statusCode == 200;
  }

  dispose() {
    client.close();
  }
}
