import 'dart:async';
import 'dart:convert';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:http/http.dart' show Client;
import 'package:tkram_delivery/app_state.dart';

class NotificationsProvider {
  final client = Client();
  final String serverToken =
      'AAAAzEnWOEE:APA91bFEytb2_H_ol2qlY4qO_1B9S9yVSVwHLLfGxJ48c0crcvXJkjZvnPmNt8mBIv9xbVzq0jXcSb2lydMrBNw2D-FPhOp8fLtHvl9yEXs1AOfm-DwS4Xf0Zi0gE1Dh2hN7InfUit0d';

  Future<void> send(
    int meId,
    String meName,
    String meImage,
    String message,
    String themId,
  ) async {
    await AppState().fbm.requestNotificationPermissions(
          IosNotificationSettings(
            sound: true,
            badge: true,
            alert: true,
            provisional: false,
          ),
        );

    final response = await client.post(
      'https://fcm.googleapis.com/fcm/send',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'key=$serverToken',
      },
      body: jsonEncode(
        {
          'notification': {'title': meName, 'body': message, 'sound': 'default'},
          'priority': 'high',
          'data': {
            'click_action': 'FLUTTER_NOTIFICATION_CLICK',
            'id': '$meId',
            'name': meName,
            'image': meImage,
          },
          // specify topic for each user!
          'to': '/topics/_users_$themId',
          //await AppState().fbm.getToken(),
        },
      ),
    );

    print(response.statusCode);
    print(response.body);
  }

  dispose() {
    client.close();
  }
}
