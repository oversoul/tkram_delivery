import 'dart:convert';

import 'package:http/http.dart' show Client;
import 'package:tkram_delivery/app_state.dart';
import 'package:tkram_delivery/models/statement.dart';

class StatementsProvider {
  Client client = Client();

  Future<StatementList> getAll() async {
    final token = AppState().prefs.getString("token");

    if (token == null) {
      return Future.error("Token was not found.");
    }

    final response = await client.get(
      "http://tkramdelivery.com/api/driver/orders",
      headers: {
        "Accept": "application/json",
        "Authorization": "Bearer $token",
      },
    );

    if (response.statusCode == 200) {
      return StatementList.fromJson(json.decode(response.body));
    }

    return Future.error(json.decode(response.body));
  }

  dispose() {
    client.close();
  }
}
