import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' show Client;
import 'package:tkram_delivery/app_state.dart';

class ContactUsProvider {
  Client client = Client();

  Future<bool> send(String name, String email, String message) async {
    final token = AppState().prefs.getString("token");

    if (token == null) {
      return Future.error("Token was not found.");
    }

    final response = await client.post(
      "http://tkramdelivery.com/api/contactus",
      body: {
        'name': name,
        'email': email,
        'message': message,
      },
      headers: {
        "Accept": "application/json",
        "Authorization": "Bearer $token",
      },
    );

    print(json.decode(response.body));
    if (response.statusCode == 200) {
      return true;
    }

    if (response.statusCode == 401 || response.statusCode == 422) {
      return Future.error(json.decode(response.body));
    }

    return Future.error({'message': 'Something went wrong!'});
  }

  dispose() {
    client.close();
  }
}
