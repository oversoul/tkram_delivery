import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' show Client;
import 'package:tkram_delivery/models/city.dart';

class CitiesProvider {
  Client client = Client();

  Future<CityList> getAll() async {
     final response = await client.get(
      "http://tkramdelivery.com/api/cities",
      headers: {
        "Accept": "application/json",
      },
    );

    if (response.statusCode == 200) {
      final body = json.decode(response.body);
      if (body['status'] != 1) {
        return Future.error(body['message']);
      }
      return CityList.fromJson(body);
    }

    return Future.error(json.decode(response.body));
  }

  dispose() {
    client.close();
  }
}
