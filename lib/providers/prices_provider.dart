import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' show Client;
import 'package:tkram_delivery/app_state.dart';
import 'package:tkram_delivery/models/price.dart';

class PricesProvider {
  Client client = Client();

  Future<PriceList> getAll() async {
    final token = AppState().prefs.getString("token");

    if (token == null) {
      return Future.error("Token was not found.");
    }

    final response = await client.get(
      "http://tkramdelivery.com/api/price",
      headers: {
        "Accept": "application/json",
        "Authorization": "Bearer $token",
      },
    );

    if (response.statusCode == 200) {
      return PriceList.fromJson(json.decode(response.body));
    }

    return Future.error(json.decode(response.body));
  }

  dispose() {
    client.close();
  }
}
