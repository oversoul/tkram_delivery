import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' show Client;
import 'package:tkram_delivery/app_state.dart';
import 'package:tkram_delivery/models/partner.dart';

class PartnersProvider {
  Client client = Client();

  Future<PartnerList> getAll() async {
    final token = AppState().prefs.getString("token");

    if (token == null) {
      return Future.error("Token was not found.");
    }

    final response = await client.get(
      "http://tkramdelivery.com/api/partners",
      headers: {
        "Accept": "application/json",
        "Authorization": "Bearer $token",
      },
    );

    if (response.statusCode == 200) {
      final body = json.decode(response.body);
      if (body['status'] != 1) {
        return Future.error(body['message']);
      }
      return PartnerList.fromJson(body);
    }

    return Future.error(json.decode(response.body));
  }

  dispose() {
    client.close();
  }
}
