import 'package:flutter/material.dart';
import 'package:tkram_delivery/app_state.dart';

class OverlayLoading extends StatefulWidget {
  @override
  _OverlayLoadingState createState() => _OverlayLoadingState();
}

class _OverlayLoadingState extends State<OverlayLoading> {
  String text;
  bool visible = false;

  @override
  void initState() {
    super.initState();
    _readFromGlobal();
    _listen();
  }

  void _readFromGlobal() {
    text = AppState().globalLoader.text;
    visible = AppState().globalLoader.visible;
  }

  void _listen() {
    appState.cmdDisplayLoader.listen((bool v) {
      setState(() {
        _readFromGlobal();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: visible,
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                color: Colors.black.withOpacity(.5),
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CircularProgressIndicator(
                  backgroundColor: Colors.white,
                ),
                SizedBox(height: 20.0),
                Text(
                  text,
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
