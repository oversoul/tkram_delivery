import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tkram_delivery/app_wrapper.dart';

void main() => runApp(MyApp());

int mainColor = 0xff1C9FAD;
Color greyColor = Color(0xFFF9F9F9);

final Map<int, Color> swatch = {
  50: Color(mainColor).withOpacity(.1),
  100: Color(mainColor).withOpacity(.2),
  200: Color(mainColor).withOpacity(.3),
  300: Color(mainColor).withOpacity(.4),
  400: Color(mainColor).withOpacity(.5),
  500: Color(mainColor).withOpacity(.6),
  600: Color(mainColor).withOpacity(.7),
  700: Color(mainColor).withOpacity(.8),
  800: Color(mainColor).withOpacity(.9),
  900: Color(mainColor).withOpacity(1),
};

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // SystemChrome.setSystemUIOverlayStyle(
    //   SystemUiOverlayStyle(statusBarColor: Colors.transparent),
    // );

    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(statusBarColor: Color(mainColor)),
    );

    return MaterialApp(
      title: 'Tkram',
      theme: ThemeData(
        appBarTheme: AppBarTheme(
          elevation: .4,
          brightness: Brightness.dark,
          color: Theme.of(context).scaffoldBackgroundColor,
          textTheme: TextTheme(
            headline6: TextStyle(
              fontSize: 17,
              color: Colors.grey,
            ),
          ),
          iconTheme: IconThemeData(
            color: Colors.grey,
          ),
          // centerTitle: true,
        ),
        buttonTheme: ButtonThemeData(
          textTheme: ButtonTextTheme.primary,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(20),
            ),
          ),
        ),
        scaffoldBackgroundColor: greyColor,
        primarySwatch: MaterialColor(mainColor, swatch),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      debugShowCheckedModeBanner: false,
      // navigatorKey: AppState().rootNavigator,
      home: AppWrapper(),
    );
  }
}