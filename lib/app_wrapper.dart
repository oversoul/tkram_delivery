import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tkram_delivery/router.dart';
import 'package:tkram_delivery/app_state.dart';
import 'package:tkram_delivery/blocs/auth_bloc.dart';
import 'package:tkram_delivery/overlay_loading.dart';

class AppWrapper extends StatefulWidget {
  @override
  _AppWrapperState createState() => _AppWrapperState();
}

class _AppWrapperState extends State<AppWrapper> with WidgetsBindingObserver {
  AuthBloc authBloc;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    authBloc = AuthBloc();
    initAuthState();
  }

  @override
  Future<bool> didPopRoute() async {
    if (AppState().rootNavigator.currentState.canPop()) {
      AppState().rootNavigator.currentState.pop();
    } else {
      final result = await showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Are you sure?'),
            content: Text('Do you want to exit the App'),
            actions: <Widget>[
              FlatButton(
                onPressed: () => Navigator.of(context).pop(true),
                child: Text('No'),
              ),
              FlatButton(
                onPressed: () {
                  SystemChannels.platform.invokeMethod('SystemNavigator.pop');
                  Navigator.of(context).pop(false);
                },
                child: Text('Yes'),
              ),
            ],
          );
        },
      );

      return result;
    }
    return true;
  }

  // @override
  // void didChangeAppLifecycleState(AppLifecycleState state) {
  //   if (state == AppLifecycleState.resumed) {
  //     WidgetsBinding.instance.removeObserver(this);
  //   }
  // }

  void initAuthState() async {
    await AppState().asyncInit();
    await authBloc.isTokenValid();

    bool isAuthenticated = await authBloc.isAuthenticated();
    AppState().evtAuthState$.add(isAuthenticated);

    AppState().configureFbm(context);
  }

  @override
  void dispose() {
    authBloc.dispose();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: AppState().globalScaffold,
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Navigator(
            onGenerateRoute: Router.generateRoute,
            key: AppState().rootNavigator,
          ),
          OverlayLoading(),
        ],
      ),
    );
  }
}
