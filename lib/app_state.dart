import 'package:rxdart/rxdart.dart';
import 'package:flutter/material.dart';
import 'package:tkram_delivery/models/user_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:tkram_delivery/providers/profile_provider.dart';

AppState appState = new AppState();

class AppState {
  static final AppState _singleton = new AppState._internal();

  factory AppState() {
    return _singleton;
  }

  AppState._internal();

  GlobalLoader globalLoader = GlobalLoader();
  GlobalKey<ScaffoldState> globalScaffold = GlobalKey<ScaffoldState>();
  GlobalKey<NavigatorState> rootNavigator = GlobalKey<NavigatorState>();

  // fbm
  final fbm = FirebaseMessaging();

  // EVENTS
  final me$ = BehaviorSubject<Profile>();
  final evtAuthState$ = PublishSubject<bool>();
  final evtUserUpdated$ = PublishSubject<int>();
  final userState$ = BehaviorSubject<UserState>();

  final profileProvider = ProfileProvider();

  // commands
  final cmdDisplayLoader = PublishSubject<bool>();

  SharedPreferences prefs;

  String currentRoute = "/";

  Future<void> asyncInit() async {
    prefs = await SharedPreferences.getInstance();
    _listenAuthState();
  }

  void configureFbm(BuildContext context) {
    fbm.requestNotificationPermissions();

    fbm.configure(onMessage: (message) {
      print("onMessage: $message, currentRoute: $currentRoute");
      // if (currentRoute == "/messages") {
      //   return;
      // }
      final String userType = prefs.getString("user_type");
      String route = userType == "driver" ? "/home" : "/drivers";

      rootNavigator.currentState.pushNamedAndRemoveUntil(route, (_) => false);

      String id = message['data']['id'];
      String name = message['data']['name'];
      String image = message['data']['image'];
      final snack = SnackBar(
        content: Text("Recieved a message from $name"),
        action: SnackBarAction(
          label: "Check",
          onPressed: () {
            rootNavigator.currentState.pushNamed(
              '/messages',
              arguments: {'id': id, 'name': name, 'image': image},
            );
          },
        ),
      );

      globalScaffold.currentState.showSnackBar(snack);
      return;
    }, onLaunch: (message) {
      print("onLunch: $message");
      String id = message['data']['id'];
      String name = message['data']['name'];
      String image = message['data']['image'];
      showDialog(
        context: rootNavigator.currentContext,
        builder: (context) => MessageRecieved(id, name, image),
      );
      return;
    }, onResume: (message) {
      print("onResume: $message");
      String id = message['data']['id'];
      String name = message['data']['name'];
      String image = message['data']['image'];

      // if (currentRoute == "/messages") {
      final String userType = prefs.getString("user_type");
      String route = userType == "driver" ? "/home" : "/drivers";

      rootNavigator.currentState.pushNamedAndRemoveUntil(route, (_) => false);
      // }

      // show popup
      showDialog(
        context: rootNavigator.currentContext,
        builder: (context) => MessageRecieved(id, name, image),
      );

      return;
    });
  }

  void fcmSubscribe(int id) {
    fbm.subscribeToTopic('_users_$id');
  }

  void fcmUnSubscribe(int id) {
    fbm.unsubscribeFromTopic('_users_$id');
  }

  _listenAuthState() {
    evtAuthState$.listen((bool state) {
      print("authState changed: $state");
      if (!state) {
        rootNavigator.currentState.pushNamedAndRemoveUntil("/", (_) => false);
      } else {
        final isConfirmed = true;

        if (!isConfirmed) {
          rootNavigator.currentState.pushNamedAndRemoveUntil(
            "/auth/confirm",
            (_) => false,
          );
        } else {
          final String userType = prefs.getString("user_type");
          String route = userType == "driver" ? "/home" : "/drivers";

          profileProvider.load().then((_v) => fcmSubscribe(me$.value.id));

          rootNavigator.currentState
              .pushNamedAndRemoveUntil(route, (_) => false);
        }
      }
    });
  }

  logout() async {
    if (me$.value != null) {
      fcmUnSubscribe(me$.value.id);
      me$.drain();
    }
    await prefs.remove("token");
    await prefs.remove("user_type");
    evtAuthState$.add(false);
  }

  dispose() {
    me$.close();
    userState$.close();
    evtAuthState$.close();
    evtUserUpdated$.close();
    cmdDisplayLoader.close();
    profileProvider.dispose();
  }
}

class GlobalLoader {
  GlobalLoader() {
    text = textDefault;
  }

  bool visible = false;

  String text;
  String textDefault = "Loading ...";

  void show([String text]) {
    visible = true;
    this.text = text ?? textDefault;
    AppState().cmdDisplayLoader.add(visible);
  }

  void hide() {
    visible = false;
    AppState().cmdDisplayLoader.add(visible);
  }
}

class MessageRecieved extends StatelessWidget {
  final String id;
  final String name;
  final String image;

  MessageRecieved(this.id, this.name, this.image);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: Text("Recieved a message from $name"),
      actions: [
        FlatButton(
          child: Text("Read it?"),
          onPressed: () {
            Navigator.of(context).pop();
            AppState().rootNavigator.currentState.pushNamed(
              '/messages',
              arguments: {'id': id, 'name': name, 'image': image},
            );
          },
        ),
        FlatButton(
          child: Text("No."),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ],
    );
  }
}
